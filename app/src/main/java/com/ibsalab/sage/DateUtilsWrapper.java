package com.ibsalab.sage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtilsWrapper {
	
	public static String datePattern ="yyyyMMdd";
	public static String dateTimePattern ="yyyy/MM/dd HH:mm:ss";
	public static String timePattern = "HHmmss";
	public static String timeHHmmPattern = "HHmm";
	public static String fileNamePattern = "yyyyMMddHHmmss";
	public static String getTimePattern() {
		return timePattern;
	}
	public static String getDatePattern() {
		return datePattern;
	}
	public static String getDateTimePattern() {
		return dateTimePattern;
	}
	
	private static SimpleDateFormat dateFormatter =new SimpleDateFormat(datePattern);
	private static SimpleDateFormat dateTimeFormatter =new SimpleDateFormat(dateTimePattern);
	private static SimpleDateFormat timeFormatter = new SimpleDateFormat(timePattern);
	private static SimpleDateFormat timeHHmmFormatter = new SimpleDateFormat(timeHHmmPattern);
	private static SimpleDateFormat fileNameFormatter = new SimpleDateFormat(fileNamePattern);
	
	public static String getDateTimeForString() throws ParseException {
		return dateTimeFormatter.format(new Date());
	} // getDate()
	
	public static String formatDate(Date date) {
		if(date == null){
			return "";
		}else{
			return dateFormatter.format(date);
		}
		
	}
	
	public static String formatDateTime(Date date) {
		return dateTimeFormatter.format(date);
	}
	
	public static String formatDateTime(long date) {
		return dateTimeFormatter.format(date);
	}
	
	public static SimpleDateFormat getDateFormatter() {
		return dateFormatter;
	}

	public static SimpleDateFormat getDateTimeFormatter() {
		return dateTimeFormatter;
	}
	
	public static String formatTime(Date date) {
		return timeFormatter.format(date);
	}

	public static String formatTimeHHmm(Date date) {
		return timeHHmmFormatter.format(date);
	}
	
	public static String getDateFileName(){
		return fileNameFormatter.format(new Date());
	}
	
	public static Date changeTimeZone(Date date, TimeZone oldZone, TimeZone newZone) {    
    	Date dateTmp = null;    
			if (date != null) {    
				int timeOffset = oldZone.getRawOffset() - newZone.getRawOffset();    
				dateTmp = new Date(date.getTime() - timeOffset);    
			}    
		return dateTmp;    
	}   
	
}
