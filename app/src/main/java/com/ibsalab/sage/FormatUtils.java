package com.ibsalab.sage;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

public class FormatUtils {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub
		String a = "U";
		try {
			System.out.println(a.getBytes("BIG5")[0]);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			System.out.println(string2Hex(a, "BIG5"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(toHexString(a.getBytes()));
		
	}
	
	public static String string2Hex(String plainText, String charset) throws UnsupportedEncodingException {
		return String.format("%040x", new BigInteger(1, plainText.getBytes(charset)));
	}
	
	public static String toHexString(byte[] ba) {
	    StringBuilder str = new StringBuilder();
	    for(int i = 0; i < ba.length; i++){
	    	String tempData = String.format("%X", ba[i]).toUpperCase();
	    	if(tempData.length() == 1){
	    		str.append("0");
	    	}
	    	str.append(tempData);
	    }
	        
	    return str.toString();
	}



}
