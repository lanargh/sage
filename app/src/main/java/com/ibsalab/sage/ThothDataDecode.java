package com.ibsalab.sage;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by t800516 on 2016/11/25.
 */

public class ThothDataDecode {
	private int heartRate = 0, SBP = 0, DBP = 0, SPTT = 0, nonFilterPPGR = 0, nonFilterPPGIR = 0, nonFilterECGL1 = 0, nonFilterECGL2 = 0, nonFilterECGL3 = 0;
	private double SPO2 = 0.0, Rvalue = 0.0, ecgRespR = 0.0, ppgRespR = 0.0, ACRatio = 0.0, DCRatio = 0.0;

	// For debug
	private static final String TAG = "AndroidDebug";

    private static final int[] ecgppgSRTable =  {64, 128, 256, 512, 1024};
    private static final float[] ecgGainTable =  {101, 46.455f};
    private static final int[] ppgGainTable =  {2000000};
    private static final int[] accSRTable =  {16, 32 ,64, 128};
    private static final float[] accLSBTable =  {3.906f, 7.813f, 15.625f, 31.250f};
    private static final int[] alsSRTable =  {};

    private static final Pattern syncPattern = Pattern.compile("3F6D3E6F4D886354");

    // Function on off
    private int ecgL1 = 0, ecgL2 = 0, ecgL3 = 0, event = 0, ppgDC = 0, ppgIR = 0, ppgRed = 0, acc = 0, bat = 0;

    // Function index(ex: sample rate)							   ppg DC sample rate
    public int ecgppgSR = 0, ppgGain = 0, accSR = 0, ppgDCSR = 0;
    private float ecgGain = 0;
    private float accLSB = 0;
    private int rtc = 0;
    private boolean dataLoss = false;

    // How much bytes a sync
    private int currentTotalBytes = 0;
    // the highest sample rate in ecg, ppg, acc, gyro
    private int highestSampleRate = 0;
    // current plot position
    private int currentPosition = 0;

    public ConcurrentLinkedQueue<Integer> ecgL1Wave;
    public ConcurrentLinkedQueue<Integer> ecgL2Wave;
    public ConcurrentLinkedQueue<Integer> ecgL3Wave;
    public ConcurrentLinkedQueue<Double> IRDCData;
    public ConcurrentLinkedQueue<Double> RDCData;
    public ConcurrentLinkedQueue<Integer> ppgIRWave;
    public ConcurrentLinkedQueue<Integer> ppgRWave;
    public ConcurrentLinkedQueue<Integer> ppgRawIRWave;
    public ConcurrentLinkedQueue<Integer> ppgRawRWave;
    private ConcurrentLinkedQueue<Integer> accXChain;
    private ConcurrentLinkedQueue<Integer> accYChain;
    private ConcurrentLinkedQueue<Integer> accZChain;

    private int[] ecgL1Point;
    private int[] ecgL2Point;
    private int[] ecgL3Point;
    private int[] ppgRPoint;
    private int[] ppgIRPoint;

    private static final byte[] PPGInvTable = {(byte) 0b00001100, (byte) 0b11110000};
    private static final byte[] ECGInvTable = {(byte) 0b00001100, (byte) 0b11110000};
    private static final byte[] ACCInvTable = {(byte) 0b00000001, (byte) 0b10001100};

    // other
    private Matcher syncMatcher;

    private static final int PPGPratio = 512;

//    public ThothTestThread canvasthread;
    // draw variable
    private int height;
    private int width;

    // filter use
    private double[] prevLpEcgPoint = new double[3];
    private double[] prevLpEcgOutput = new double[3];
    private double[] prevHpEcgPoint = new double[3];
    private double[] prevHpEcgOutput = new double[3];
    private double[] prevLpPpgPoint = new double[2];
    private double[] prevLpPpgOutput = new double[2];
    private double[] prevHpPpgPoint = new double[2];
    private double[] prevHpPpgOutput = new double[2];
    private double[] prevAccPoint = new double[3];
    private double[] prevAccOutput = new double[3];

    private StringBuffer holeString = new StringBuffer();

    private float[] amp = {1, 1};
    
    // 2017/07/25 added. Use function type(ECG+PPG/HOLTER/PPG) for more accurate analysis.
    // 0 = ECG+PPG; 1 = HOLTER; 2 = PPG; default = 0.
    private int functionType = 0;

    public ThothDataDecode() {
        ecgL1Wave = new ConcurrentLinkedQueue<Integer>();
        ecgL2Wave = new ConcurrentLinkedQueue<Integer>();
        ecgL3Wave = new ConcurrentLinkedQueue<Integer>();
        IRDCData = new ConcurrentLinkedQueue<Double>();
        RDCData = new ConcurrentLinkedQueue<Double>();
        ppgIRWave = new ConcurrentLinkedQueue<Integer>();
        ppgRWave = new ConcurrentLinkedQueue<Integer>();
        ppgRawIRWave = new ConcurrentLinkedQueue<Integer>();
        ppgRawRWave = new ConcurrentLinkedQueue<Integer>();
        accXChain = new ConcurrentLinkedQueue();
        accYChain = new ConcurrentLinkedQueue();
        accZChain = new ConcurrentLinkedQueue();
    }

    public void updateData(String notifyData) {
        holeString.append(notifyData);
        syncMatcher = syncPattern.matcher(holeString);
        if(syncMatcher.find() && (holeString.length() > (syncMatcher.end() + 24))) {
        	//Log.d("ThothDataDecode", "syncMatcher");
            // check data length and decode data
            if(syncMatcher.start() == currentTotalBytes * 2) {
            	//Log.d("ThothDataDecode", "decode");
                decodeData(holeString.substring(0, syncMatcher.start()));
                //writeFile(outputStream);
            }
            decodeFunction(holeString.substring(syncMatcher.end(), syncMatcher.end() + 8));
            decodeFuncIndex(holeString.substring(syncMatcher.end() + 8, syncMatcher.end() + 16));
            decodeRTC(holeString.substring(syncMatcher.end() + 16, syncMatcher.end() + 24));
            currentTotalBytes = calTotalBytes();
            highestSampleRate = calHighestSampleRate();
            // remove decoded data
            holeString.delete(0, syncMatcher.end() + 24);
        }
    }

	public void decodeFunction(String inputFuncString) {
		Integer input = new BigInteger(inputFuncString, 16).intValue();
		
		ppgDC = (input >> 31) & 1;
		ecgL1 = (input >> 30) & 1;
		ecgL2 = (input >> 29) & 1;
		ecgL3 = (input >> 28) & 1;
		event = (input >> 25) & 1;
		ppgIR = (input >> 23) & 1;
		ppgRed = (input >> 22) & 1;
		acc = (input >> 21) & 1;
		bat = (input >> 16) & 1;
	}

    public void decodeFuncIndex(String inputFuncFreqString) {
        Integer input = new BigInteger(inputFuncFreqString, 16).intValue();

        ecgppgSR = ecgppgSRTable[(input >> 28) & 15];
        ecgGain = ecgGainTable[(input >> 26) & 3];
        //ppgGain = ppgGainTable[(input >> 24) & 3];
        accSR = accSRTable[(input >> 22) & 3];
        accLSB = accLSBTable[(input >> 20) & 3];
        // it is 16 times ppg dc per second, so divide 16 by highest sample rate to determine dc's count. 
        ppgDCSR = ecgppgSR/16;
        //Log.d("ThothDataDecode", String.valueOf(ecgppgSR) + " " + String.valueOf(ppgDCSR));
    }
    
    public void decodeRTC(String inputFuncFreqString) {
    	rtc = new BigInteger(inputFuncFreqString, 16).intValue();
    	//Log.d("ThothDataDecode", String.valueOf(rtc));
    }

    // Count how much bytes a sync have.
    public int calTotalBytes() {
        int output = 0;
        
        if(ecgL1 == 1)
            output = output + ecgppgSR * 2;
        if(ecgL2 == 1)
            output = output + ecgppgSR * 2;
        if(ecgL3 == 1)
            output = output + ecgppgSR * 2;
        if(ppgDC == 1)
        	output = output + 16 * 4;
        if(ppgIR == 1)
            output = output + ecgppgSR * 2;
        if(ppgRed == 1)
            output = output + ecgppgSR * 2;
        if(acc == 1)
            output = output + accSR * 6;
        if(bat == 1)
            output = output + 6;
        
        //Log.d("ThothDataDecode", String.valueOf(output));
        return output;
    }

    public int calHighestSampleRate() {
        if((ecgL1 | ecgL2 | ecgL3 | ppgIR | ppgRed) == 1)
            return ecgppgSR;
        else if(acc == 1)
            return  accSR;
        else
            return 0;
    }

    public void decodeData(String inputData) {
    	int lead1Raw = 0, lead2Raw = 0, lead3Raw = 0, irRaw = 0, redRaw = 0;
    	double  irDC = 0, rDC = 0;
        // remove last of string
        if(bat == 1)
            inputData = inputData.substring(0, inputData.length() - 12);

        // iterator all data to fill wave list
        for(int i = 1; i <= highestSampleRate; i++) {
            if(ecgL1 == 1) {
            	lead1Raw = parseECG(inputData.substring(0, 4), 0);
                ecgL1Wave.add(lead1Raw);
                inputData = inputData.substring(4, inputData.length());
            }
            if(ecgL2 == 1) {
            	lead2Raw = parseECG(inputData.substring(0, 4), 1);
                ecgL2Wave.add(lead2Raw);
                inputData = inputData.substring(4, inputData.length());
            }
            if(ecgL3 == 1) {
            	lead3Raw = parseECG(inputData.substring(0, 4), 2);
                ecgL3Wave.add(lead3Raw);
                inputData = inputData.substring(4, inputData.length());
            }
            
            // Take off Pre_P_IR and Pre_P_R from first packet in each second.
            if(ppgDC == 1 && ((i % ppgDCSR) == 1)) {
            	irDC = parseDC(inputData.substring(0, 4));
            	//Log.d("ThothDataDecode", String.valueOf(i));
            	IRDCData.add(irDC);
            	inputData = inputData.substring(4, inputData.length());

            	rDC = parseDC(inputData.substring(0, 4));
				RDCData.add(rDC);
				inputData = inputData.substring(4, inputData.length());
            }
			
            if(ppgIR == 1) {
            	irRaw = parsePPG(inputData.substring(0, 4), 0);
                ppgIRWave.add(irRaw * (-1));
                ppgRawIRWave.add(nonFilterPPGIR);
                inputData = inputData.substring(4, inputData.length());
            }
            if(ppgRed == 1) {
            	redRaw = parsePPG(inputData.substring(0, 4), 1);
                ppgRWave.add(redRaw * (-1));
                ppgRawRWave.add(nonFilterPPGR);
                inputData = inputData.substring(4, inputData.length());
            }
            if(acc == 1 && (i%(highestSampleRate/accSR) == 0)) {
                //accXChain.add(parseACC(inputData.substring(0, 4), 0));
                //accYChain.add(parseACC(inputData.substring(4, 8), 1));
                //accZChain.add(parseACC(inputData.substring(8, 12), 2));
                inputData = inputData.substring(12, inputData.length());
            }
            
            irDC = 0;
            rDC = 0;
            //Log.d("ThothDataDecode", String.valueOf(Rvalue));
        }
    }
    
    public void clearQueue() {
    	if(ecgL1Wave.size() > 2560)
    		ecgL1Wave.clear();
    	if(ecgL2Wave.size() > 2560)
    		ecgL2Wave.clear();
    	if(ecgL3Wave.size() > 2560)
    		ecgL3Wave.clear();
    	if(IRDCData.size() > 2560)
    		IRDCData.clear();
    	if(RDCData.size() > 2560)
    		RDCData.clear();
    	if(ppgIRWave.size() > 2560)
    		ppgIRWave.clear();
    	if(ppgRWave.size() > 2560)
    		ppgRWave.clear();
    	if(ppgRawIRWave.size() > 2560)
    		ppgRawIRWave.clear();
    	if(ppgRawRWave.size() > 2560)
    		ppgRawRWave.clear();
    	if(accXChain.size() > 2560)
    		accXChain.clear();
    	if(accYChain.size() > 2560)
    		accYChain.clear();
    	if(accZChain.size() > 2560)
    		accZChain.clear();
    }

    public int parseECG(String input, int type) {
        int output = 0;
        int mv8, mv0;
        Byte[] byteValue;

        double diff = 0, switchHpTemp = 0.0, switchLpTemp = 0.0;

        byteValue = hexStringTo2Byte(input);

        // inverse byte
        mv8 = ((ECGInvTable[0] ^ byteValue[0]) & 0x0f) << 8;
        mv0 = (ECGInvTable[1] ^ byteValue[1]) & 0xff;

        // value
        output = mv8 | mv0;

        diff = output;
        diff = ((diff * 3 / 4095) / ecgGain) * 1000000; // * 2.4178242
        if(type == 0)
        	nonFilterECGL1 = (int)diff;
        else if(type == 1)
        	nonFilterECGL2 = (int)diff;
        else
        	nonFilterECGL3 = (int)diff;
        // diff = -diff * 73.85/ecgGain; // uV

        // high pass filter
        switchHpTemp = diff;
        diff = diff - prevHpEcgPoint[type] + 0.97 * prevHpEcgOutput[type];
        prevHpEcgOutput[type] = diff;
        prevHpEcgPoint[type] = switchHpTemp;
        
        // low pass filter
        switchLpTemp = diff;
        diff = 0.319988923452122 * diff + 0.319988923452122 * prevLpEcgPoint[type] + 0.360022153095757 * prevLpEcgOutput[type];
        prevLpEcgOutput[type] = diff;
        prevLpEcgPoint[type] = switchLpTemp;
        

        return  (int) (diff * 1);
    }
    
    public double parseDC(String input) {
    	double output = 0;
    	int mv8, mv0;
    	Byte[] byteValue;
    	
    	byteValue = hexStringTo2Byte(input);
    	
    	mv8 = ((PPGInvTable[0] ^ byteValue[0]) & 0x0f) << 8;
        mv0 = (PPGInvTable[1] ^ byteValue[1]) & 0xff;
    	
    	output = mv8 | mv0;
    	output = ((output * 3 / 4095) * 1000 - 134) * 10;
    	
    	return output;
    }

    public int parsePPG(String input, int type) {
        int output = 0;
        int mv8, mv0;
        Byte[] byteValue;

        double diff = 0.0, switchHpTemp = 0.0, switchLpTemp = 0.0, rawDC = 0.0;
        byteValue = hexStringTo2Byte(input);

        // inverse byte
        mv8 = ((PPGInvTable[0] ^ byteValue[0]) & 0x0f) << 8;
        mv0 = (PPGInvTable[1] ^ byteValue[1]) & 0xff;

        // value
        output = mv8 | mv0;
        
        diff = output; // inverse PPG signal - chi ho 20161201
        
        rawDC = (diff * 3 / 4095) * 10000;
        if(type == 0)
        	nonFilterPPGIR = (int)rawDC;
        else
        	nonFilterPPGR = (int)rawDC;
        
        diff = (diff * 3 / 4095) * 1000000;

        // high pass filter
        switchHpTemp = diff;
        diff = diff - prevHpPpgPoint[type] + 0.99 * prevHpPpgOutput[type];
        prevHpPpgOutput[type] = diff;
        prevHpPpgPoint[type] = switchHpTemp;
        
        // low pass filter
        switchLpTemp = diff;
        diff = 0.0468264154904264 * diff + 0.0468264154904264 * prevLpPpgPoint[type] + 0.906347169019147 * prevLpPpgOutput[type];
        prevLpPpgOutput[type] = diff;
        prevLpPpgPoint[type] = switchLpTemp;

        output = (int) (diff);

        return  (int)(output * amp[type]);
    }

    private int parseACC(String input, int type) {
        int output = 0;
        byte signByte;
        int mv8, mv0;
        Byte[] byteValue;

        double diff = 0, switchTemp = 0;

        byteValue = hexStringTo2Byte(input);
        signByte = (byte) ((byteValue[0] & 0b00000010) >> 1);

        // inverse byte
        mv8 = ((ACCInvTable[0] ^ byteValue[0]) & 0x01) << 8;
        mv0 = (ACCInvTable[1] ^ byteValue[1]) & 0xff;

        // value
        output = mv8 | mv0;
        diff = output;

        diff = diff * accLSB/1000; // mg

        if(signByte == 1)
            diff = diff * -1;

        return  (int) (diff);
    }

    private Byte[] hexStringTo3Byte(String input) {
        Byte[] output = new Byte[3];
        output[0] = (byte) Integer.parseInt(input.substring(0,2), 16);
        output[1] = (byte) Integer.parseInt(input.substring(2,4), 16);
        output[2] = (byte) Integer.parseInt(input.substring(4,6), 16);
        return output;
    }

    private Byte[] hexStringTo2Byte(String input) {
        Byte[] output = new Byte[2];
        output[0] = (byte) Integer.parseInt(input.substring(0,2), 16);
        output[1] = (byte) Integer.parseInt(input.substring(2,4), 16);
        return output;
    }

    private String byteArrayToString(Byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X", b));
        }

        return sb.toString();
    }

    public void setViewSize(int h, int w)
    {
        height = h;
        width = w;
    }
    
    public void setFunctionType(int type) {
    	functionType = type;
    }

    public void upPPGRAmp() {
        amp[1] = amp[1] * 2;
    }
    
	public void downPPGRAmp() {
        amp[1] = amp[1] / 2;
    }
	
	public void upPPGIRAmp() {
        amp[0] = amp[0] * 2;
    }
    
	public void downPPGIRAmp() {
        amp[0] = amp[0] / 2;
    }

    private void writeFile(FileOutputStream outputStream) {
        try {
            outputStream.write(ecgL1Wave.toString().getBytes());
            //outputStream.write("\r\n".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    
	public int getHeartRate() {
		if(heartRate < 0)
			return 0;
		else
			return heartRate;
	}
	
	public double getSPO2Value() {
		return SPO2;
	}
	
	public double getRValue() {
		return Rvalue;
	}
	
	public int getSBPValue() {
		return SBP;
	}
	
	public int getDBPValue() {
		return DBP;
	}
	
	public int getSPTTValue() {
		return SPTT;
	}
	
	public double getECGRespRValue() {
		return ecgRespR;
	}
	
	public double getPPGRespRValue() {
		return ppgRespR;
	}
	
	public String getACRatioValue() {
		DecimalFormat df = new DecimalFormat("0.000");
		return df.format(ACRatio);
	}
	
	public String getDCRatioValue() {
		DecimalFormat df = new DecimalFormat("0.000");
		return df.format(DCRatio);
	}
}
