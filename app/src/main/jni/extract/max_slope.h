/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * max_slope.h
 *
 * Code generation for function 'max_slope'
 *
 */

#ifndef MAX_SLOPE_H
#define MAX_SLOPE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern void max_slope(const emxArray_real_T *s, double min_value, double
                      max_value, emxArray_real_T *op);

#endif

/* End of code generation (max_slope.h) */
