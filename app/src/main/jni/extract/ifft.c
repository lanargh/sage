/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ifft.c
 *
 * Code generation for function 'ifft'
 *
 */

/* Include files */
#include "ifft.h"
#include "extract.h"
#include "extract_emxutil.h"
#include "fft1.h"
#include "rt_nonfinite.h"
#include <math.h>

/* Function Definitions */
void ifft(const emxArray_creal_T *x, emxArray_creal_T *y)
{
  emxArray_creal_T *b_y;
  int n;
  emxArray_real_T *costab1q;
  emxArray_real_T *costab;
  emxArray_real_T *sintab;
  emxArray_real_T *sintabinv;
  emxArray_creal_T *wwc;
  emxArray_creal_T *fv;
  emxArray_creal_T *b_fv;
  boolean_T useRadix2;
  int i;
  int N2blue;
  int nd2;
  double nt_im;
  int rt;
  int k;
  emxArray_creal_T b_x;
  int c_x[1];
  int idx;
  int nInt2;
  int c_y;
  double nt_re;
  double y_tmp_im;
  emxInit_creal_T(&b_y, 1);
  n = x->size[1];
  emxInit_real_T(&costab1q, 2);
  emxInit_real_T(&costab, 2);
  emxInit_real_T(&sintab, 2);
  emxInit_real_T(&sintabinv, 2);
  emxInit_creal_T(&wwc, 1);
  emxInit_creal_T(&fv, 1);
  emxInit_creal_T(&b_fv, 1);
  if (x->size[1] == 0) {
    i = b_y->size[0];
    b_y->size[0] = x->size[1];
    emxEnsureCapacity_creal_T(b_y, i);
  } else {
    useRadix2 = ((x->size[1] & (x->size[1] - 1)) == 0);
    get_algo_sizes(x->size[1], useRadix2, &N2blue, &nd2);
    nt_im = 6.2831853071795862 / (double)nd2;
    rt = nd2 / 2 / 2;
    i = costab1q->size[0] * costab1q->size[1];
    costab1q->size[0] = 1;
    costab1q->size[1] = rt + 1;
    emxEnsureCapacity_real_T(costab1q, i);
    costab1q->data[0] = 1.0;
    nd2 = rt / 2 - 1;
    for (k = 0; k <= nd2; k++) {
      costab1q->data[k + 1] = cos(nt_im * ((double)k + 1.0));
    }

    i = nd2 + 2;
    nd2 = rt - 1;
    for (k = i; k <= nd2; k++) {
      costab1q->data[k] = sin(nt_im * (double)(rt - k));
    }

    costab1q->data[rt] = 0.0;
    if (!useRadix2) {
      rt = costab1q->size[1] - 1;
      nd2 = (costab1q->size[1] - 1) << 1;
      i = costab->size[0] * costab->size[1];
      costab->size[0] = 1;
      costab->size[1] = nd2 + 1;
      emxEnsureCapacity_real_T(costab, i);
      i = sintab->size[0] * sintab->size[1];
      sintab->size[0] = 1;
      sintab->size[1] = nd2 + 1;
      emxEnsureCapacity_real_T(sintab, i);
      costab->data[0] = 1.0;
      sintab->data[0] = 0.0;
      i = sintabinv->size[0] * sintabinv->size[1];
      sintabinv->size[0] = 1;
      sintabinv->size[1] = nd2 + 1;
      emxEnsureCapacity_real_T(sintabinv, i);
      for (k = 0; k < rt; k++) {
        sintabinv->data[k + 1] = costab1q->data[(rt - k) - 1];
      }

      i = costab1q->size[1];
      for (k = i; k <= nd2; k++) {
        sintabinv->data[k] = costab1q->data[k - rt];
      }

      for (k = 0; k < rt; k++) {
        costab->data[k + 1] = costab1q->data[k + 1];
        sintab->data[k + 1] = -costab1q->data[(rt - k) - 1];
      }

      i = costab1q->size[1];
      for (k = i; k <= nd2; k++) {
        costab->data[k] = -costab1q->data[nd2 - k];
        sintab->data[k] = -costab1q->data[k - rt];
      }
    } else {
      rt = costab1q->size[1] - 1;
      nd2 = (costab1q->size[1] - 1) << 1;
      i = costab->size[0] * costab->size[1];
      costab->size[0] = 1;
      costab->size[1] = nd2 + 1;
      emxEnsureCapacity_real_T(costab, i);
      i = sintab->size[0] * sintab->size[1];
      sintab->size[0] = 1;
      sintab->size[1] = nd2 + 1;
      emxEnsureCapacity_real_T(sintab, i);
      costab->data[0] = 1.0;
      sintab->data[0] = 0.0;
      for (k = 0; k < rt; k++) {
        costab->data[k + 1] = costab1q->data[k + 1];
        sintab->data[k + 1] = costab1q->data[(rt - k) - 1];
      }

      i = costab1q->size[1];
      for (k = i; k <= nd2; k++) {
        costab->data[k] = -costab1q->data[nd2 - k];
        sintab->data[k] = costab1q->data[k - rt];
      }

      sintabinv->size[0] = 1;
      sintabinv->size[1] = 0;
    }

    if (useRadix2) {
      nd2 = x->size[1];
      b_x = *x;
      c_x[0] = nd2;
      b_x.size = &c_x[0];
      b_x.numDimensions = 1;
      r2br_r2dit_trig_impl(&b_x, x->size[1], costab, sintab, b_y);
      if (b_y->size[0] > 1) {
        nt_im = 1.0 / (double)b_y->size[0];
        nd2 = b_y->size[0];
        for (i = 0; i < nd2; i++) {
          b_y->data[i].re *= nt_im;
          b_y->data[i].im *= nt_im;
        }
      }
    } else {
      nd2 = (x->size[1] + x->size[1]) - 1;
      i = wwc->size[0];
      wwc->size[0] = nd2;
      emxEnsureCapacity_creal_T(wwc, i);
      idx = x->size[1];
      rt = 0;
      wwc->data[x->size[1] - 1].re = 1.0;
      wwc->data[x->size[1] - 1].im = 0.0;
      nInt2 = x->size[1] << 1;
      i = x->size[1];
      for (k = 0; k <= i - 2; k++) {
        c_y = ((k + 1) << 1) - 1;
        if (nInt2 - rt <= c_y) {
          rt += c_y - nInt2;
        } else {
          rt += c_y;
        }

        nt_im = 3.1415926535897931 * (double)rt / (double)n;
        if (nt_im == 0.0) {
          nt_re = 1.0;
          nt_im = 0.0;
        } else {
          nt_re = cos(nt_im);
          nt_im = sin(nt_im);
        }

        wwc->data[idx - 2].re = nt_re;
        wwc->data[idx - 2].im = -nt_im;
        idx--;
      }

      idx = 0;
      i = nd2 - 1;
      for (k = i; k >= n; k--) {
        wwc->data[k] = wwc->data[idx];
        idx++;
      }

      idx = x->size[1];
      i = b_y->size[0];
      b_y->size[0] = x->size[1];
      emxEnsureCapacity_creal_T(b_y, i);
      nd2 = 0;
      for (k = 0; k < idx; k++) {
        rt = (n + k) - 1;
        nt_re = wwc->data[rt].re;
        nt_im = wwc->data[rt].im;
        b_y->data[k].re = nt_re * x->data[nd2].re + nt_im * x->data[nd2].im;
        b_y->data[k].im = nt_re * x->data[nd2].im - nt_im * x->data[nd2].re;
        nd2++;
      }

      i = x->size[1] + 1;
      for (k = i; k <= n; k++) {
        b_y->data[k - 1].re = 0.0;
        b_y->data[k - 1].im = 0.0;
      }

      r2br_r2dit_trig_impl(b_y, N2blue, costab, sintab, fv);
      r2br_r2dit_trig_impl(wwc, N2blue, costab, sintab, b_fv);
      i = b_fv->size[0];
      b_fv->size[0] = fv->size[0];
      emxEnsureCapacity_creal_T(b_fv, i);
      nd2 = fv->size[0];
      for (i = 0; i < nd2; i++) {
        nt_im = fv->data[i].re * b_fv->data[i].im + fv->data[i].im * b_fv->
          data[i].re;
        b_fv->data[i].re = fv->data[i].re * b_fv->data[i].re - fv->data[i].im *
          b_fv->data[i].im;
        b_fv->data[i].im = nt_im;
      }

      r2br_r2dit_trig_impl(b_fv, N2blue, costab, sintabinv, fv);
      if (fv->size[0] > 1) {
        nt_im = 1.0 / (double)fv->size[0];
        nd2 = fv->size[0];
        for (i = 0; i < nd2; i++) {
          fv->data[i].re *= nt_im;
          fv->data[i].im *= nt_im;
        }
      }

      idx = 0;
      nt_re = x->size[1];
      i = wwc->size[0];
      for (k = n; k <= i; k++) {
        nt_im = wwc->data[k - 1].re * fv->data[k - 1].re + wwc->data[k - 1].im *
          fv->data[k - 1].im;
        y_tmp_im = wwc->data[k - 1].re * fv->data[k - 1].im - wwc->data[k - 1].
          im * fv->data[k - 1].re;
        b_y->data[idx].re = nt_im;
        b_y->data[idx].im = y_tmp_im;
        b_y->data[idx].re = nt_im;
        b_y->data[idx].im = y_tmp_im;
        if (b_y->data[idx].im == 0.0) {
          y_tmp_im = b_y->data[idx].re / nt_re;
          nt_im = 0.0;
        } else if (b_y->data[idx].re == 0.0) {
          y_tmp_im = 0.0;
          nt_im = b_y->data[idx].im / nt_re;
        } else {
          y_tmp_im = b_y->data[idx].re / nt_re;
          nt_im = b_y->data[idx].im / nt_re;
        }

        b_y->data[idx].re = y_tmp_im;
        b_y->data[idx].im = nt_im;
        idx++;
      }
    }
  }

  emxFree_creal_T(&b_fv);
  emxFree_creal_T(&fv);
  emxFree_creal_T(&wwc);
  emxFree_real_T(&sintabinv);
  emxFree_real_T(&sintab);
  emxFree_real_T(&costab);
  emxFree_real_T(&costab1q);
  i = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = x->size[1];
  emxEnsureCapacity_creal_T(y, i);
  nd2 = x->size[1];
  for (i = 0; i < nd2; i++) {
    y->data[i] = b_y->data[i];
  }

  emxFree_creal_T(&b_y);
}

/* End of code generation (ifft.c) */
