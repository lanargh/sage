/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * vmedian.h
 *
 * Code generation for function 'vmedian'
 *
 */

#ifndef VMEDIAN_H
#define VMEDIAN_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern double vmedian(emxArray_real_T *v, int n);

#endif

/* End of code generation (vmedian.h) */
