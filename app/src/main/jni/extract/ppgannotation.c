/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ppgannotation.c
 *
 * Code generation for function 'ppgannotation'
 *
 */

/* Include files */
#include "ppgannotation.h"
#include "circshift.h"
#include "diff.h"
#include "extract.h"
#include "extract_data.h"
#include "extract_emxutil.h"
#include "extract_rtwutil.h"
#include "find.h"
#include "interp1.h"
#include "linspace.h"
#include "mean.h"
#include "nullAssignment.h"
#include "rt_nonfinite.h"
#include "vmedian.h"
#include <math.h>
#include <stdio.h>

/* Function Declarations */
static void BP_lowpass(const emxArray_real_T *waveform, emxArray_real_T
  *filtwaveform);
static void FixIndex(const emxArray_real_T *BrokeIndex, const emxArray_real_T
                     *Signal, emxArray_real_T *fixedIndex);
static void b_FixIndex(const emxArray_real_T *BrokeIndex, const emxArray_real_T *
  Signal, double minWavelength, emxArray_real_T *fixedIndex);
static void c_FixIndex(const emxArray_real_T *BrokeIndex, const emxArray_real_T *
  Signal, double minWavelength, emxArray_real_T *fixedIndex);
static void d_FixIndex(double BrokeIndex, const emxArray_real_T *Signal,
  emxArray_real_T *fixedIndex);
static void e_FixIndex(double BrokeIndex, const emxArray_real_T *Signal,
  emxArray_real_T *fixedIndex);
static void getDicroticIndex(const emxArray_real_T *waveformDD, const
  emxArray_real_T *waveformD, const emxArray_real_T *bpwaveform, const
  emxArray_real_T *footIndex, emxArray_real_T *systolicIndex, emxArray_real_T
  *dicroticIndex, emxArray_real_T *notchIndex);
static void getFootIndex(const emxArray_real_T *bpwaveform, const
  emxArray_real_T *waveformDDPlus, const emxArray_boolean_T *zoneOfInterest,
  emxArray_real_T *footIndex);
static void rollingWindow(const emxArray_real_T *vector, double winsize,
  emxArray_real_T *rwin);
static void winmean(const emxArray_real_T *rwin, emxArray_real_T *res);
static void winsum(const emxArray_real_T *rwin, emxArray_real_T *res);

/* Function Definitions */
static void BP_lowpass(const emxArray_real_T *waveform, emxArray_real_T
  *filtwaveform)
{
  int nA;
  int nC;
  int filtwaveform_tmp;
  emxArray_real_T *buffer;
  int k;
  static const double dv[13] = { 0.027777777777777776, 0.055555555555555552,
    0.083333333333333329, 0.1111111111111111, 0.1388888888888889,
    0.16666666666666669, 0.13888888888888892, 0.11111111111111116,
    0.0833333333333334, 0.055555555555555636, 0.027777777777777873,
    1.1102230246251565E-16, 1.2490009027033011E-16 };

  /* LOWPASS Filter input signal (200 Hz assumed) */
  /*  Multiply by 36 to remove DC gain */
  nA = waveform->size[1] - 1;
  if (waveform->size[1] == 0) {
    nC = 13;
  } else {
    nC = waveform->size[1] + 12;
  }

  filtwaveform_tmp = filtwaveform->size[0] * filtwaveform->size[1];
  filtwaveform->size[0] = 1;
  filtwaveform->size[1] = nC;
  emxEnsureCapacity_real_T(filtwaveform, filtwaveform_tmp);
  for (filtwaveform_tmp = 0; filtwaveform_tmp < nC; filtwaveform_tmp++) {
    filtwaveform->data[filtwaveform_tmp] = 0.0;
  }

  if (waveform->size[1] > 0) {
    if (13 > waveform->size[1]) {
      for (k = 0; k <= nA; k++) {
        for (nC = 0; nC < 13; nC++) {
          filtwaveform_tmp = k + nC;
          filtwaveform->data[filtwaveform_tmp] += waveform->data[k] * dv[nC];
        }
      }
    } else {
      for (k = 0; k < 13; k++) {
        for (nC = 0; nC <= nA; nC++) {
          filtwaveform_tmp = k + nC;
          filtwaveform->data[filtwaveform_tmp] += dv[k] * waveform->data[nC];
        }
      }
    }
  }

  emxInit_real_T(&buffer, 2);
  filtwaveform_tmp = buffer->size[0] * buffer->size[1];
  buffer->size[0] = 1;
  nC = (int)floor((double)filtwaveform->size[1] / 2.0);
  buffer->size[1] = nC;
  emxEnsureCapacity_real_T(buffer, filtwaveform_tmp);
  for (filtwaveform_tmp = 0; filtwaveform_tmp < nC; filtwaveform_tmp++) {
    buffer->data[filtwaveform_tmp] = 0.0;
  }

  nC = filtwaveform->size[1] - 5;
  if (filtwaveform->size[1] > 1) {
    for (k = 0; k < 5; k++) {
      buffer->data[k] = filtwaveform->data[k];
    }

    filtwaveform_tmp = filtwaveform->size[1];
    for (k = 0; k <= filtwaveform_tmp - 6; k++) {
      filtwaveform->data[k] = filtwaveform->data[k + 5];
    }

    for (k = 0; k < 5; k++) {
      filtwaveform->data[k + nC] = buffer->data[k];
    }
  }

  emxFree_real_T(&buffer);
  for (filtwaveform_tmp = 0; filtwaveform_tmp < 5; filtwaveform_tmp++) {
    filtwaveform->data[filtwaveform_tmp] = rtNaN;
  }

  filtwaveform_tmp = filtwaveform->size[0] * filtwaveform->size[1];
  if (1 > waveform->size[1]) {
    filtwaveform->size[1] = 0;
  } else {
    filtwaveform->size[1] = waveform->size[1];
  }

  emxEnsureCapacity_real_T(filtwaveform, filtwaveform_tmp);
}

static void FixIndex(const emxArray_real_T *BrokeIndex, const emxArray_real_T
                     *Signal, emxArray_real_T *fixedIndex)
{
  int i;
  int loop_ub;
  emxArray_boolean_T *r;
  double OldIndex;
  double d;
  double NewIndex;
  int i1;
  int i2;
  int n_tmp;
  int idx;
  int k;
  boolean_T exitg1;
  double ex;

  /*  follows a slope either up or down in a given window until an */
  /*  extremum is attained */
  i = fixedIndex->size[0] * fixedIndex->size[1];
  fixedIndex->size[0] = 1;
  fixedIndex->size[1] = BrokeIndex->size[1];
  emxEnsureCapacity_real_T(fixedIndex, i);
  loop_ub = BrokeIndex->size[0] * BrokeIndex->size[1];
  for (i = 0; i < loop_ub; i++) {
    fixedIndex->data[i] = BrokeIndex->data[i];
  }

  i = BrokeIndex->size[1];
  for (loop_ub = 0; loop_ub < i; loop_ub++) {
    OldIndex = BrokeIndex->data[loop_ub];
    if (OldIndex + 10.0 < Signal->size[1]) {
      d = fmax(OldIndex - 3.0, 1.0);
      NewIndex = fmin(OldIndex + 3.0, Signal->size[1]);
      if (d > NewIndex) {
        i1 = -1;
        i2 = -1;
      } else {
        i1 = (int)d - 2;
        i2 = (int)NewIndex - 1;
      }

      n_tmp = i2 - i1;
      if (n_tmp <= 2) {
        if (n_tmp == 1) {
          idx = 1;
        } else {
          d = Signal->data[i1 + 2];
          if ((Signal->data[i1 + 1] > d) || (rtIsNaN(Signal->data[i1 + 1]) &&
               (!rtIsNaN(d)))) {
            idx = 2;
          } else {
            idx = 1;
          }
        }
      } else {
        if (!rtIsNaN(Signal->data[i1 + 1])) {
          idx = 1;
        } else {
          idx = 0;
          k = 2;
          exitg1 = false;
          while ((!exitg1) && (k <= n_tmp)) {
            if (!rtIsNaN(Signal->data[i1 + k])) {
              idx = k;
              exitg1 = true;
            } else {
              k++;
            }
          }
        }

        if (idx == 0) {
          idx = 1;
        } else {
          ex = Signal->data[i1 + idx];
          i2 = idx + 1;
          for (k = i2; k <= n_tmp; k++) {
            d = Signal->data[i1 + k];
            if (ex > d) {
              ex = d;
              idx = k;
            }
          }
        }
      }

      NewIndex = (((double)idx + BrokeIndex->data[loop_ub]) - 3.0) - 1.0;
      while (!(OldIndex == NewIndex)) {
        OldIndex = NewIndex;
        d = fmax(NewIndex - 3.0, 1.0);
        NewIndex = fmin(NewIndex + 3.0, Signal->size[1]);
        if (d > NewIndex) {
          i1 = -1;
          i2 = -1;
        } else {
          i1 = (int)d - 2;
          i2 = (int)NewIndex - 1;
        }

        n_tmp = i2 - i1;
        if (n_tmp <= 2) {
          if (n_tmp == 1) {
            idx = 1;
          } else {
            NewIndex = Signal->data[i1 + 2];
            if ((Signal->data[i1 + 1] > NewIndex) || (rtIsNaN(Signal->data[i1 +
                  1]) && (!rtIsNaN(NewIndex)))) {
              idx = 2;
            } else {
              idx = 1;
            }
          }
        } else {
          if (!rtIsNaN(Signal->data[i1 + 1])) {
            idx = 1;
          } else {
            idx = 0;
            k = 2;
            exitg1 = false;
            while ((!exitg1) && (k <= n_tmp)) {
              if (!rtIsNaN(Signal->data[i1 + k])) {
                idx = k;
                exitg1 = true;
              } else {
                k++;
              }
            }
          }

          if (idx == 0) {
            idx = 1;
          } else {
            ex = Signal->data[i1 + idx];
            i2 = idx + 1;
            for (k = i2; k <= n_tmp; k++) {
              NewIndex = Signal->data[i1 + k];
              if (ex > NewIndex) {
                ex = NewIndex;
                idx = k;
              }
            }
          }
        }

        NewIndex = ((double)idx + d) - 1.0;
      }

      fixedIndex->data[loop_ub] = NewIndex;
    }
  }

  emxInit_boolean_T(&r, 2);
  i = r->size[0] * r->size[1];
  r->size[0] = 1;
  r->size[1] = fixedIndex->size[1];
  emxEnsureCapacity_boolean_T(r, i);
  loop_ub = fixedIndex->size[0] * fixedIndex->size[1];
  for (i = 0; i < loop_ub; i++) {
    r->data[i] = (fixedIndex->data[i] > Signal->size[1]);
  }

  nullAssignment(fixedIndex, r);
  i = r->size[0] * r->size[1];
  r->size[0] = 1;
  r->size[1] = fixedIndex->size[1];
  emxEnsureCapacity_boolean_T(r, i);
  loop_ub = fixedIndex->size[0] * fixedIndex->size[1];
  for (i = 0; i < loop_ub; i++) {
    r->data[i] = (fixedIndex->data[i] < 1.0);
  }

  nullAssignment(fixedIndex, r);
  emxFree_boolean_T(&r);
}

static void b_FixIndex(const emxArray_real_T *BrokeIndex, const emxArray_real_T *
  Signal, double minWavelength, emxArray_real_T *fixedIndex)
{
  int i;
  int loop_ub;
  double Radius;
  emxArray_boolean_T *r;
  double OldIndex;
  double d;
  double NewIndex;
  int i1;
  int i2;
  int n_tmp;
  int idx;
  int k;
  boolean_T exitg1;
  double ex;

  /*  follows a slope either up or down in a given window until an */
  /*  extremum is attained */
  i = fixedIndex->size[0] * fixedIndex->size[1];
  fixedIndex->size[0] = 1;
  fixedIndex->size[1] = BrokeIndex->size[1];
  emxEnsureCapacity_real_T(fixedIndex, i);
  loop_ub = BrokeIndex->size[0] * BrokeIndex->size[1];
  for (i = 0; i < loop_ub; i++) {
    fixedIndex->data[i] = BrokeIndex->data[i];
  }

  Radius = rt_roundd_snf(minWavelength / 4.0);
  i = BrokeIndex->size[1];
  for (loop_ub = 0; loop_ub < i; loop_ub++) {
    OldIndex = BrokeIndex->data[loop_ub];
    if (OldIndex + rt_roundd_snf(minWavelength) < Signal->size[1]) {
      d = fmax(OldIndex - Radius, 1.0);
      NewIndex = fmin(OldIndex + Radius, Signal->size[1]);
      if (d > NewIndex) {
        i1 = -1;
        i2 = -1;
      } else {
        i1 = (int)d - 2;
        i2 = (int)NewIndex - 1;
      }

      n_tmp = i2 - i1;
      if (n_tmp <= 2) {
        if (n_tmp == 1) {
          idx = 1;
        } else {
          d = Signal->data[i1 + 2];
          if ((Signal->data[i1 + 1] < d) || (rtIsNaN(Signal->data[i1 + 1]) &&
               (!rtIsNaN(d)))) {
            idx = 2;
          } else {
            idx = 1;
          }
        }
      } else {
        if (!rtIsNaN(Signal->data[i1 + 1])) {
          idx = 1;
        } else {
          idx = 0;
          k = 2;
          exitg1 = false;
          while ((!exitg1) && (k <= n_tmp)) {
            if (!rtIsNaN(Signal->data[i1 + k])) {
              idx = k;
              exitg1 = true;
            } else {
              k++;
            }
          }
        }

        if (idx == 0) {
          idx = 1;
        } else {
          ex = Signal->data[i1 + idx];
          i2 = idx + 1;
          for (k = i2; k <= n_tmp; k++) {
            d = Signal->data[i1 + k];
            if (ex < d) {
              ex = d;
              idx = k;
            }
          }
        }
      }

      NewIndex = (((double)idx + BrokeIndex->data[loop_ub]) - Radius) - 1.0;
      while (!(OldIndex == NewIndex)) {
        OldIndex = NewIndex;
        d = fmax(NewIndex - Radius, 1.0);
        NewIndex = fmin(NewIndex + Radius, Signal->size[1]);
        if (d > NewIndex) {
          i1 = -1;
          i2 = -1;
        } else {
          i1 = (int)d - 2;
          i2 = (int)NewIndex - 1;
        }

        n_tmp = i2 - i1;
        if (n_tmp <= 2) {
          if (n_tmp == 1) {
            idx = 1;
          } else {
            NewIndex = Signal->data[i1 + 2];
            if ((Signal->data[i1 + 1] < NewIndex) || (rtIsNaN(Signal->data[i1 +
                  1]) && (!rtIsNaN(NewIndex)))) {
              idx = 2;
            } else {
              idx = 1;
            }
          }
        } else {
          if (!rtIsNaN(Signal->data[i1 + 1])) {
            idx = 1;
          } else {
            idx = 0;
            k = 2;
            exitg1 = false;
            while ((!exitg1) && (k <= n_tmp)) {
              if (!rtIsNaN(Signal->data[i1 + k])) {
                idx = k;
                exitg1 = true;
              } else {
                k++;
              }
            }
          }

          if (idx == 0) {
            idx = 1;
          } else {
            ex = Signal->data[i1 + idx];
            i2 = idx + 1;
            for (k = i2; k <= n_tmp; k++) {
              NewIndex = Signal->data[i1 + k];
              if (ex < NewIndex) {
                ex = NewIndex;
                idx = k;
              }
            }
          }
        }

        NewIndex = ((double)idx + d) - 1.0;
      }

      fixedIndex->data[loop_ub] = NewIndex;
    }
  }

  emxInit_boolean_T(&r, 2);
  i = r->size[0] * r->size[1];
  r->size[0] = 1;
  r->size[1] = fixedIndex->size[1];
  emxEnsureCapacity_boolean_T(r, i);
  loop_ub = fixedIndex->size[0] * fixedIndex->size[1];
  for (i = 0; i < loop_ub; i++) {
    r->data[i] = (fixedIndex->data[i] > Signal->size[1]);
  }

  nullAssignment(fixedIndex, r);
  i = r->size[0] * r->size[1];
  r->size[0] = 1;
  r->size[1] = fixedIndex->size[1];
  emxEnsureCapacity_boolean_T(r, i);
  loop_ub = fixedIndex->size[0] * fixedIndex->size[1];
  for (i = 0; i < loop_ub; i++) {
    r->data[i] = (fixedIndex->data[i] < 1.0);
  }

  nullAssignment(fixedIndex, r);
  emxFree_boolean_T(&r);
}

static void c_FixIndex(const emxArray_real_T *BrokeIndex, const emxArray_real_T *
  Signal, double minWavelength, emxArray_real_T *fixedIndex)
{
  int i;
  int loop_ub;
  double Radius;
  emxArray_boolean_T *r;
  double OldIndex;
  double d;
  double NewIndex;
  int i1;
  int i2;
  int n_tmp;
  int idx;
  int k;
  boolean_T exitg1;
  double ex;

  /*  follows a slope either up or down in a given window until an */
  /*  extremum is attained */
  i = fixedIndex->size[0] * fixedIndex->size[1];
  fixedIndex->size[0] = 1;
  fixedIndex->size[1] = BrokeIndex->size[1];
  emxEnsureCapacity_real_T(fixedIndex, i);
  loop_ub = BrokeIndex->size[0] * BrokeIndex->size[1];
  for (i = 0; i < loop_ub; i++) {
    fixedIndex->data[i] = BrokeIndex->data[i];
  }

  Radius = rt_roundd_snf(minWavelength / 4.0);
  i = BrokeIndex->size[1];
  for (loop_ub = 0; loop_ub < i; loop_ub++) {
    OldIndex = BrokeIndex->data[loop_ub];
    if (OldIndex + rt_roundd_snf(minWavelength) < Signal->size[1]) {
      d = fmax(OldIndex - Radius, 1.0);
      NewIndex = fmin(OldIndex + Radius, Signal->size[1]);
      if (d > NewIndex) {
        i1 = -1;
        i2 = -1;
      } else {
        i1 = (int)d - 2;
        i2 = (int)NewIndex - 1;
      }

      n_tmp = i2 - i1;
      if (n_tmp <= 2) {
        if (n_tmp == 1) {
          idx = 1;
        } else {
          d = Signal->data[i1 + 2];
          if ((Signal->data[i1 + 1] > d) || (rtIsNaN(Signal->data[i1 + 1]) &&
               (!rtIsNaN(d)))) {
            idx = 2;
          } else {
            idx = 1;
          }
        }
      } else {
        if (!rtIsNaN(Signal->data[i1 + 1])) {
          idx = 1;
        } else {
          idx = 0;
          k = 2;
          exitg1 = false;
          while ((!exitg1) && (k <= n_tmp)) {
            if (!rtIsNaN(Signal->data[i1 + k])) {
              idx = k;
              exitg1 = true;
            } else {
              k++;
            }
          }
        }

        if (idx == 0) {
          idx = 1;
        } else {
          ex = Signal->data[i1 + idx];
          i2 = idx + 1;
          for (k = i2; k <= n_tmp; k++) {
            d = Signal->data[i1 + k];
            if (ex > d) {
              ex = d;
              idx = k;
            }
          }
        }
      }

      NewIndex = (((double)idx + BrokeIndex->data[loop_ub]) - Radius) - 1.0;
      while (!(OldIndex == NewIndex)) {
        OldIndex = NewIndex;
        d = fmax(NewIndex - Radius, 1.0);
        NewIndex = fmin(NewIndex + Radius, Signal->size[1]);
        if (d > NewIndex) {
          i1 = -1;
          i2 = -1;
        } else {
          i1 = (int)d - 2;
          i2 = (int)NewIndex - 1;
        }

        n_tmp = i2 - i1;
        if (n_tmp <= 2) {
          if (n_tmp == 1) {
            idx = 1;
          } else {
            NewIndex = Signal->data[i1 + 2];
            if ((Signal->data[i1 + 1] > NewIndex) || (rtIsNaN(Signal->data[i1 +
                  1]) && (!rtIsNaN(NewIndex)))) {
              idx = 2;
            } else {
              idx = 1;
            }
          }
        } else {
          if (!rtIsNaN(Signal->data[i1 + 1])) {
            idx = 1;
          } else {
            idx = 0;
            k = 2;
            exitg1 = false;
            while ((!exitg1) && (k <= n_tmp)) {
              if (!rtIsNaN(Signal->data[i1 + k])) {
                idx = k;
                exitg1 = true;
              } else {
                k++;
              }
            }
          }

          if (idx == 0) {
            idx = 1;
          } else {
            ex = Signal->data[i1 + idx];
            i2 = idx + 1;
            for (k = i2; k <= n_tmp; k++) {
              NewIndex = Signal->data[i1 + k];
              if (ex > NewIndex) {
                ex = NewIndex;
                idx = k;
              }
            }
          }
        }

        NewIndex = ((double)idx + d) - 1.0;
      }

      fixedIndex->data[loop_ub] = NewIndex;
    }
  }

  emxInit_boolean_T(&r, 2);
  i = r->size[0] * r->size[1];
  r->size[0] = 1;
  r->size[1] = fixedIndex->size[1];
  emxEnsureCapacity_boolean_T(r, i);
  loop_ub = fixedIndex->size[0] * fixedIndex->size[1];
  for (i = 0; i < loop_ub; i++) {
    r->data[i] = (fixedIndex->data[i] > Signal->size[1]);
  }

  nullAssignment(fixedIndex, r);
  i = r->size[0] * r->size[1];
  r->size[0] = 1;
  r->size[1] = fixedIndex->size[1];
  emxEnsureCapacity_boolean_T(r, i);
  loop_ub = fixedIndex->size[0] * fixedIndex->size[1];
  for (i = 0; i < loop_ub; i++) {
    r->data[i] = (fixedIndex->data[i] < 1.0);
  }

  nullAssignment(fixedIndex, r);
  emxFree_boolean_T(&r);
}

static void d_FixIndex(double BrokeIndex, const emxArray_real_T *Signal,
  emxArray_real_T *fixedIndex)
{
  int i;
  double OldIndex;
  emxArray_boolean_T *r;
  double d;
  double NewIndex;
  int loop_ub;
  int n_tmp;
  int idx;
  int k;
  boolean_T exitg1;
  double ex;

  /*  follows a slope either up or down in a given window until an */
  /*  extremum is attained */
  i = fixedIndex->size[0] * fixedIndex->size[1];
  fixedIndex->size[0] = 1;
  fixedIndex->size[1] = 1;
  emxEnsureCapacity_real_T(fixedIndex, i);
  fixedIndex->data[0] = BrokeIndex;
  if (BrokeIndex + 4.0 < Signal->size[1]) {
    OldIndex = BrokeIndex;
    d = fmax(BrokeIndex - 1.0, 1.0);
    NewIndex = fmin(BrokeIndex + 1.0, Signal->size[1]);
    if (d > NewIndex) {
      i = -1;
      loop_ub = -1;
    } else {
      i = (int)d - 2;
      loop_ub = (int)NewIndex - 1;
    }

    n_tmp = loop_ub - i;
    if (n_tmp <= 2) {
      if (n_tmp == 1) {
        idx = 1;
      } else {
        d = Signal->data[i + 2];
        if ((Signal->data[i + 1] > d) || (rtIsNaN(Signal->data[i + 1]) &&
             (!rtIsNaN(d)))) {
          idx = 2;
        } else {
          idx = 1;
        }
      }
    } else {
      if (!rtIsNaN(Signal->data[i + 1])) {
        idx = 1;
      } else {
        idx = 0;
        k = 2;
        exitg1 = false;
        while ((!exitg1) && (k <= n_tmp)) {
          if (!rtIsNaN(Signal->data[i + k])) {
            idx = k;
            exitg1 = true;
          } else {
            k++;
          }
        }
      }

      if (idx == 0) {
        idx = 1;
      } else {
        ex = Signal->data[i + idx];
        loop_ub = idx + 1;
        for (k = loop_ub; k <= n_tmp; k++) {
          d = Signal->data[i + k];
          if (ex > d) {
            ex = d;
            idx = k;
          }
        }
      }
    }

    NewIndex = (((double)idx + BrokeIndex) - 1.0) - 1.0;
    while (!(OldIndex == NewIndex)) {
      OldIndex = NewIndex;
      d = fmax(NewIndex - 1.0, 1.0);
      NewIndex = fmin(NewIndex + 1.0, Signal->size[1]);
      if (d > NewIndex) {
        i = -1;
        loop_ub = -1;
      } else {
        i = (int)d - 2;
        loop_ub = (int)NewIndex - 1;
      }

      n_tmp = loop_ub - i;
      if (n_tmp <= 2) {
        if (n_tmp == 1) {
          idx = 1;
        } else {
          NewIndex = Signal->data[i + 2];
          if ((Signal->data[i + 1] > NewIndex) || (rtIsNaN(Signal->data[i + 1]) &&
               (!rtIsNaN(NewIndex)))) {
            idx = 2;
          } else {
            idx = 1;
          }
        }
      } else {
        if (!rtIsNaN(Signal->data[i + 1])) {
          idx = 1;
        } else {
          idx = 0;
          k = 2;
          exitg1 = false;
          while ((!exitg1) && (k <= n_tmp)) {
            if (!rtIsNaN(Signal->data[i + k])) {
              idx = k;
              exitg1 = true;
            } else {
              k++;
            }
          }
        }

        if (idx == 0) {
          idx = 1;
        } else {
          ex = Signal->data[i + idx];
          loop_ub = idx + 1;
          for (k = loop_ub; k <= n_tmp; k++) {
            NewIndex = Signal->data[i + k];
            if (ex > NewIndex) {
              ex = NewIndex;
              idx = k;
            }
          }
        }
      }

      NewIndex = ((double)idx + d) - 1.0;
    }

    fixedIndex->data[0] = NewIndex;
  }

  emxInit_boolean_T(&r, 2);
  i = r->size[0] * r->size[1];
  r->size[0] = 1;
  r->size[1] = 1;
  emxEnsureCapacity_boolean_T(r, i);
  for (i = 0; i < 1; i++) {
    r->data[0] = (fixedIndex->data[0] > Signal->size[1]);
  }

  nullAssignment(fixedIndex, r);
  i = r->size[0] * r->size[1];
  r->size[0] = 1;
  r->size[1] = fixedIndex->size[1];
  emxEnsureCapacity_boolean_T(r, i);
  loop_ub = fixedIndex->size[0] * fixedIndex->size[1];
  for (i = 0; i < loop_ub; i++) {
    r->data[i] = (fixedIndex->data[i] < 1.0);
  }

  nullAssignment(fixedIndex, r);
  emxFree_boolean_T(&r);
}

static void e_FixIndex(double BrokeIndex, const emxArray_real_T *Signal,
  emxArray_real_T *fixedIndex)
{
  int i;
  double OldIndex;
  emxArray_boolean_T *r;
  double d;
  double NewIndex;
  int loop_ub;
  int n_tmp;
  int idx;
  int k;
  boolean_T exitg1;
  double ex;

  /*  follows a slope either up or down in a given window until an */
  /*  extremum is attained */
  i = fixedIndex->size[0] * fixedIndex->size[1];
  fixedIndex->size[0] = 1;
  fixedIndex->size[1] = 1;
  emxEnsureCapacity_real_T(fixedIndex, i);
  fixedIndex->data[0] = BrokeIndex;
  if (BrokeIndex + 4.0 < Signal->size[1]) {
    OldIndex = BrokeIndex;
    d = fmax(BrokeIndex - 1.0, 1.0);
    NewIndex = fmin(BrokeIndex + 1.0, Signal->size[1]);
    if (d > NewIndex) {
      i = -1;
      loop_ub = -1;
    } else {
      i = (int)d - 2;
      loop_ub = (int)NewIndex - 1;
    }

    n_tmp = loop_ub - i;
    if (n_tmp <= 2) {
      if (n_tmp == 1) {
        idx = 1;
      } else {
        d = Signal->data[i + 2];
        if ((Signal->data[i + 1] < d) || (rtIsNaN(Signal->data[i + 1]) &&
             (!rtIsNaN(d)))) {
          idx = 2;
        } else {
          idx = 1;
        }
      }
    } else {
      if (!rtIsNaN(Signal->data[i + 1])) {
        idx = 1;
      } else {
        idx = 0;
        k = 2;
        exitg1 = false;
        while ((!exitg1) && (k <= n_tmp)) {
          if (!rtIsNaN(Signal->data[i + k])) {
            idx = k;
            exitg1 = true;
          } else {
            k++;
          }
        }
      }

      if (idx == 0) {
        idx = 1;
      } else {
        ex = Signal->data[i + idx];
        loop_ub = idx + 1;
        for (k = loop_ub; k <= n_tmp; k++) {
          d = Signal->data[i + k];
          if (ex < d) {
            ex = d;
            idx = k;
          }
        }
      }
    }

    NewIndex = (((double)idx + BrokeIndex) - 1.0) - 1.0;
    while (!(OldIndex == NewIndex)) {
      OldIndex = NewIndex;
      d = fmax(NewIndex - 1.0, 1.0);
      NewIndex = fmin(NewIndex + 1.0, Signal->size[1]);
      if (d > NewIndex) {
        i = -1;
        loop_ub = -1;
      } else {
        i = (int)d - 2;
        loop_ub = (int)NewIndex - 1;
      }

      n_tmp = loop_ub - i;
      if (n_tmp <= 2) {
        if (n_tmp == 1) {
          idx = 1;
        } else {
          NewIndex = Signal->data[i + 2];
          if ((Signal->data[i + 1] < NewIndex) || (rtIsNaN(Signal->data[i + 1]) &&
               (!rtIsNaN(NewIndex)))) {
            idx = 2;
          } else {
            idx = 1;
          }
        }
      } else {
        if (!rtIsNaN(Signal->data[i + 1])) {
          idx = 1;
        } else {
          idx = 0;
          k = 2;
          exitg1 = false;
          while ((!exitg1) && (k <= n_tmp)) {
            if (!rtIsNaN(Signal->data[i + k])) {
              idx = k;
              exitg1 = true;
            } else {
              k++;
            }
          }
        }

        if (idx == 0) {
          idx = 1;
        } else {
          ex = Signal->data[i + idx];
          loop_ub = idx + 1;
          for (k = loop_ub; k <= n_tmp; k++) {
            NewIndex = Signal->data[i + k];
            if (ex < NewIndex) {
              ex = NewIndex;
              idx = k;
            }
          }
        }
      }

      NewIndex = ((double)idx + d) - 1.0;
    }

    fixedIndex->data[0] = NewIndex;
  }

  emxInit_boolean_T(&r, 2);
  i = r->size[0] * r->size[1];
  r->size[0] = 1;
  r->size[1] = 1;
  emxEnsureCapacity_boolean_T(r, i);
  for (i = 0; i < 1; i++) {
    r->data[0] = (fixedIndex->data[0] > Signal->size[1]);
  }

  nullAssignment(fixedIndex, r);
  i = r->size[0] * r->size[1];
  r->size[0] = 1;
  r->size[1] = fixedIndex->size[1];
  emxEnsureCapacity_boolean_T(r, i);
  loop_ub = fixedIndex->size[0] * fixedIndex->size[1];
  for (i = 0; i < loop_ub; i++) {
    r->data[i] = (fixedIndex->data[i] < 1.0);
  }

  nullAssignment(fixedIndex, r);
  emxFree_boolean_T(&r);
}

static void getDicroticIndex(const emxArray_real_T *waveformDD, const
  emxArray_real_T *waveformD, const emxArray_real_T *bpwaveform, const
  emxArray_real_T *footIndex, emxArray_real_T *systolicIndex, emxArray_real_T
  *dicroticIndex, emxArray_real_T *notchIndex)
{
  int i;
  int i1;
  emxArray_real_T *notQuiteSystolicIndex;
  int i2;
  int loop_ub;
  emxArray_real_T *x;
  double ndbl;
  emxArray_real_T *straightLines;
  double minWavelength;
  int varargin_1_idx_1;
  int b_i;
  double d;
  double d1;
  double slope;
  double intercept;
  int nm1d2;
  double apnd;
  double cdiff;
  int n;
  emxArray_int32_T *ii;
  int x_tmp;
  double d2;
  emxArray_boolean_T *b_x;
  boolean_T exitg1;
  if (2 > footIndex->size[1]) {
    i = 0;
    i1 = 0;
  } else {
    i = 1;
    i1 = footIndex->size[1];
  }

  emxInit_real_T(&notQuiteSystolicIndex, 2);
  i2 = notQuiteSystolicIndex->size[0] * notQuiteSystolicIndex->size[1];
  notQuiteSystolicIndex->size[0] = 1;
  loop_ub = i1 - i;
  notQuiteSystolicIndex->size[1] = loop_ub;
  emxEnsureCapacity_real_T(notQuiteSystolicIndex, i2);
  for (i1 = 0; i1 < loop_ub; i1++) {
    notQuiteSystolicIndex->data[i1] = footIndex->data[i + i1] - footIndex->
      data[i1];
  }

  emxInit_real_T(&x, 2);
  if (notQuiteSystolicIndex->size[1] == 0) {
    ndbl = rtNaN;
  } else {
    i = x->size[0] * x->size[1];
    x->size[0] = 1;
    x->size[1] = notQuiteSystolicIndex->size[1];
    emxEnsureCapacity_real_T(x, i);
    loop_ub = notQuiteSystolicIndex->size[0] * notQuiteSystolicIndex->size[1];
    for (i = 0; i < loop_ub; i++) {
      x->data[i] = notQuiteSystolicIndex->data[i];
    }

    ndbl = vmedian(x, notQuiteSystolicIndex->size[1]);
  }

  emxInit_real_T(&straightLines, 2);

  /*  This assumes steady heartrate */
  minWavelength = rt_roundd_snf(ndbl / Fs / 5.0 * Fs);
  varargin_1_idx_1 = bpwaveform->size[1];
  i = straightLines->size[0] * straightLines->size[1];
  straightLines->size[0] = 1;
  loop_ub = bpwaveform->size[1];
  straightLines->size[1] = loop_ub;
  emxEnsureCapacity_real_T(straightLines, i);
  for (i = 0; i < loop_ub; i++) {
    straightLines->data[i] = 0.0;
  }

  b_FixIndex(systolicIndex, bpwaveform, minWavelength, notQuiteSystolicIndex);
  i = footIndex->size[1];
  for (b_i = 0; b_i <= i - 2; b_i++) {
    /*  compute the parameters of the straight line going from systole to */
    /*  diastole */
    d = footIndex->data[b_i + 1];
    d1 = notQuiteSystolicIndex->data[b_i];
    ndbl = bpwaveform->data[(int)d - 1];
    loop_ub = (int)d1;
    slope = (ndbl - bpwaveform->data[loop_ub - 1]) / (d - d1);
    intercept = ndbl - slope * d;
    ndbl = footIndex->data[b_i];
    if (ndbl > d1) {
      i2 = 0;
      loop_ub = 0;
      i1 = 1;
    } else {
      i1 = (int)ndbl;
      i2 = i1 - 1;
    }

    loop_ub -= i2;
    for (nm1d2 = 0; nm1d2 < loop_ub; nm1d2++) {
      straightLines->data[(i1 + nm1d2) - 1] = bpwaveform->data[i2 + nm1d2];
    }

    d1 = notQuiteSystolicIndex->data[b_i];
    if (d1 > d) {
      i1 = 1;
    } else {
      i1 = (int)d1;
    }

    if (rtIsNaN(d1) || rtIsNaN(d)) {
      i2 = x->size[0] * x->size[1];
      x->size[0] = 1;
      x->size[1] = 1;
      emxEnsureCapacity_real_T(x, i2);
      x->data[0] = rtNaN;
    } else if (d < d1) {
      x->size[0] = 1;
      x->size[1] = 0;
    } else if ((rtIsInf(d1) || rtIsInf(d)) && (d1 == d)) {
      i2 = x->size[0] * x->size[1];
      x->size[0] = 1;
      x->size[1] = 1;
      emxEnsureCapacity_real_T(x, i2);
      x->data[0] = rtNaN;
    } else if (floor(d1) == d1) {
      i2 = x->size[0] * x->size[1];
      x->size[0] = 1;
      loop_ub = (int)floor(d - d1);
      x->size[1] = loop_ub + 1;
      emxEnsureCapacity_real_T(x, i2);
      for (i2 = 0; i2 <= loop_ub; i2++) {
        x->data[i2] = notQuiteSystolicIndex->data[b_i] + (double)i2;
      }
    } else {
      ndbl = floor((d - d1) + 0.5);
      apnd = d1 + ndbl;
      cdiff = apnd - d;
      if (fabs(cdiff) < 4.4408920985006262E-16 * fmax(fabs(d1), fabs(d))) {
        ndbl++;
        apnd = d;
      } else if (cdiff > 0.0) {
        apnd = d1 + (ndbl - 1.0);
      } else {
        ndbl++;
      }

      if (ndbl >= 0.0) {
        n = (int)ndbl;
      } else {
        n = 0;
      }

      i2 = x->size[0] * x->size[1];
      x->size[0] = 1;
      x->size[1] = n;
      emxEnsureCapacity_real_T(x, i2);
      if (n > 0) {
        x->data[0] = d1;
        if (n > 1) {
          x->data[n - 1] = apnd;
          nm1d2 = (n - 1) / 2;
          for (loop_ub = 0; loop_ub <= nm1d2 - 2; loop_ub++) {
            x_tmp = loop_ub + 1;
            x->data[loop_ub + 1] = notQuiteSystolicIndex->data[b_i] + (double)
              x_tmp;
            x->data[(n - loop_ub) - 2] = apnd - (double)x_tmp;
          }

          if (nm1d2 << 1 == n - 1) {
            x->data[nm1d2] = (notQuiteSystolicIndex->data[b_i] + apnd) / 2.0;
          } else {
            x->data[nm1d2] = notQuiteSystolicIndex->data[b_i] + (double)nm1d2;
            x->data[nm1d2 + 1] = apnd - (double)nm1d2;
          }
        }
      }
    }

    loop_ub = x->size[1];
    for (i2 = 0; i2 < loop_ub; i2++) {
      straightLines->data[(i1 + i2) - 1] = slope * x->data[i2] + intercept;
    }
  }

  i = straightLines->size[0] * straightLines->size[1];
  straightLines->size[0] = 1;
  straightLines->size[1] = bpwaveform->size[1];
  emxEnsureCapacity_real_T(straightLines, i);
  loop_ub = bpwaveform->size[0] * bpwaveform->size[1] - 1;
  for (i = 0; i <= loop_ub; i++) {
    straightLines->data[i] = bpwaveform->data[i] - straightLines->data[i];
  }

  if (footIndex->data[footIndex->size[1] - 1] > waveformDD->size[1]) {
    i = 0;
    i1 = 0;
  } else {
    i = (int)footIndex->data[footIndex->size[1] - 1] - 1;
    i1 = waveformDD->size[1];
  }

  if (footIndex->data[footIndex->size[1] - 1] > straightLines->size[1]) {
    i2 = 1;
  } else {
    i2 = (int)footIndex->data[footIndex->size[1] - 1];
  }

  loop_ub = i1 - i;
  for (i1 = 0; i1 < loop_ub; i1++) {
    straightLines->data[(i2 + i1) - 1] = waveformDD->data[i + i1];
  }

  i = notQuiteSystolicIndex->size[0] * notQuiteSystolicIndex->size[1];
  notQuiteSystolicIndex->size[0] = 1;
  notQuiteSystolicIndex->size[1] = systolicIndex->size[1];
  emxEnsureCapacity_real_T(notQuiteSystolicIndex, i);
  loop_ub = systolicIndex->size[0] * systolicIndex->size[1];
  for (i = 0; i < loop_ub; i++) {
    notQuiteSystolicIndex->data[i] = systolicIndex->data[i] + minWavelength;
  }

  c_FixIndex(notQuiteSystolicIndex, straightLines, minWavelength / 4.0,
             notchIndex);
  ndbl = rt_roundd_snf(0.25 * minWavelength);
  i = notQuiteSystolicIndex->size[0] * notQuiteSystolicIndex->size[1];
  notQuiteSystolicIndex->size[0] = 1;
  notQuiteSystolicIndex->size[1] = notchIndex->size[1];
  emxEnsureCapacity_real_T(notQuiteSystolicIndex, i);
  loop_ub = notchIndex->size[0] * notchIndex->size[1];
  emxFree_real_T(&straightLines);
  for (i = 0; i < loop_ub; i++) {
    notQuiteSystolicIndex->data[i] = notchIndex->data[i] + ndbl;
  }

  c_FixIndex(notQuiteSystolicIndex, waveformDD, rt_roundd_snf(0.25 *
              minWavelength), dicroticIndex);
  if (1 > dicroticIndex->size[1]) {
    i = 0;
  } else {
    i = dicroticIndex->size[1];
  }

  i1 = systolicIndex->size[0] * systolicIndex->size[1];
  systolicIndex->size[1] = i;
  emxEnsureCapacity_real_T(systolicIndex, i1);

  /*  if a local minimum and maximum exist, move the dicrotic indices to */
  /*  these */
  emxFree_real_T(&notQuiteSystolicIndex);
  emxInit_int32_T(&ii, 2);
  if (0 <= i - 1) {
    d2 = rt_roundd_snf(minWavelength / 4.0);
    varargin_1_idx_1 = waveformD->size[1];
  }

  emxInit_boolean_T(&b_x, 2);
  for (b_i = 0; b_i < i; b_i++) {
    ndbl = systolicIndex->data[b_i] + rt_roundd_snf(minWavelength / 2.0);
    apnd = dicroticIndex->data[b_i] + d2;
    if ((apnd > varargin_1_idx_1) || rtIsNaN(apnd)) {
      apnd = varargin_1_idx_1;
    }

    if (ndbl > apnd) {
      i1 = -1;
      i2 = -1;
    } else {
      i1 = (int)ndbl - 2;
      i2 = (int)apnd - 1;
    }

    i2 -= i1;
    if (2 > i2) {
      nm1d2 = 0;
      i2 = 0;
    } else {
      nm1d2 = 1;
    }

    x_tmp = b_x->size[0] * b_x->size[1];
    b_x->size[0] = 1;
    loop_ub = i2 - nm1d2;
    b_x->size[1] = loop_ub;
    emxEnsureCapacity_boolean_T(b_x, x_tmp);
    for (i2 = 0; i2 < loop_ub; i2++) {
      b_x->data[i2] = (waveformD->data[((i1 + nm1d2) + i2) + 1] *
                       waveformD->data[(i1 + i2) + 1] < 0.0);
    }

    i1 = b_x->size[1];
    nm1d2 = 0;
    i2 = ii->size[0] * ii->size[1];
    ii->size[0] = 1;
    ii->size[1] = b_x->size[1];
    emxEnsureCapacity_int32_T(ii, i2);
    loop_ub = 0;
    exitg1 = false;
    while ((!exitg1) && (loop_ub <= i1 - 1)) {
      if (b_x->data[loop_ub]) {
        nm1d2++;
        ii->data[nm1d2 - 1] = loop_ub + 1;
        if (nm1d2 >= i1) {
          exitg1 = true;
        } else {
          loop_ub++;
        }
      } else {
        loop_ub++;
      }
    }

    if (b_x->size[1] == 1) {
      if (nm1d2 == 0) {
        ii->size[1] = 0;
      }
    } else {
      i1 = ii->size[0] * ii->size[1];
      if (1 > nm1d2) {
        ii->size[1] = 0;
      } else {
        ii->size[1] = nm1d2;
      }

      emxEnsureCapacity_int32_T(ii, i1);
    }

    if (ii->size[1] >= 2) {
      d_FixIndex(notchIndex->data[b_i], bpwaveform, x);
      notchIndex->data[b_i] = x->data[0];
      e_FixIndex(fmin(notchIndex->data[b_i] + rt_roundd_snf(0.25 * minWavelength),
                      bpwaveform->size[1]), bpwaveform, x);
      dicroticIndex->data[b_i] = x->data[0];
    }
  }

  emxFree_real_T(&x);
  emxFree_int32_T(&ii);
  emxFree_boolean_T(&b_x);
}

static void getFootIndex(const emxArray_real_T *bpwaveform, const
  emxArray_real_T *waveformDDPlus, const emxArray_boolean_T *zoneOfInterest,
  emxArray_real_T *footIndex)
{
  int dimSize;
  emxArray_real_T *zoneWall_tmp;
  int tmp1;
  emxArray_boolean_T *b_zoneWall_tmp;
  int i;
  int ixLead;
  int iyLead;
  int work_data_idx_0;
  int m;
  emxArray_int32_T *BP_start;
  emxArray_int32_T *BP_stop;
  emxArray_int32_T *b_BP_stop;
  double d;
  boolean_T exitg1;
  double ex;
  dimSize = zoneOfInterest->size[1];
  emxInit_real_T(&zoneWall_tmp, 2);
  if (zoneOfInterest->size[1] == 0) {
    zoneWall_tmp->size[0] = 1;
    zoneWall_tmp->size[1] = 0;
  } else {
    tmp1 = zoneOfInterest->size[1] - 1;
    if (tmp1 >= 1) {
      tmp1 = 1;
    }

    if (tmp1 < 1) {
      zoneWall_tmp->size[0] = 1;
      zoneWall_tmp->size[1] = 0;
    } else {
      i = zoneWall_tmp->size[0] * zoneWall_tmp->size[1];
      zoneWall_tmp->size[0] = 1;
      zoneWall_tmp->size[1] = zoneOfInterest->size[1] - 1;
      emxEnsureCapacity_real_T(zoneWall_tmp, i);
      if (zoneOfInterest->size[1] - 1 != 0) {
        ixLead = 1;
        iyLead = 0;
        work_data_idx_0 = zoneOfInterest->data[0];
        for (m = 2; m <= dimSize; m++) {
          tmp1 = zoneOfInterest->data[ixLead];
          i = tmp1;
          tmp1 -= work_data_idx_0;
          work_data_idx_0 = i;
          ixLead++;
          zoneWall_tmp->data[iyLead] = tmp1;
          iyLead++;
        }
      }
    }
  }

  emxInit_boolean_T(&b_zoneWall_tmp, 2);

  /*  Remove leading falling edges */
  i = b_zoneWall_tmp->size[0] * b_zoneWall_tmp->size[1];
  b_zoneWall_tmp->size[0] = 1;
  b_zoneWall_tmp->size[1] = zoneWall_tmp->size[1];
  emxEnsureCapacity_boolean_T(b_zoneWall_tmp, i);
  tmp1 = zoneWall_tmp->size[0] * zoneWall_tmp->size[1];
  for (i = 0; i < tmp1; i++) {
    b_zoneWall_tmp->data[i] = (zoneWall_tmp->data[i] == 1.0);
  }

  emxInit_int32_T(&BP_start, 2);
  emxInit_int32_T(&BP_stop, 2);
  eml_find(b_zoneWall_tmp, BP_stop);
  i = BP_start->size[0] * BP_start->size[1];
  BP_start->size[0] = 1;
  BP_start->size[1] = BP_stop->size[1];
  emxEnsureCapacity_int32_T(BP_start, i);
  tmp1 = BP_stop->size[0] * BP_stop->size[1];
  for (i = 0; i < tmp1; i++) {
    BP_start->data[i] = BP_stop->data[i];
  }

  i = b_zoneWall_tmp->size[0] * b_zoneWall_tmp->size[1];
  b_zoneWall_tmp->size[0] = 1;
  b_zoneWall_tmp->size[1] = zoneWall_tmp->size[1];
  emxEnsureCapacity_boolean_T(b_zoneWall_tmp, i);
  tmp1 = zoneWall_tmp->size[0] * zoneWall_tmp->size[1];
  for (i = 0; i < tmp1; i++) {
    b_zoneWall_tmp->data[i] = (zoneWall_tmp->data[i] == -1.0);
  }

  eml_find(b_zoneWall_tmp, BP_stop);

  /*  Remove leading falling edges */
  emxFree_boolean_T(&b_zoneWall_tmp);
  emxInit_int32_T(&b_BP_stop, 2);
  while (BP_stop->data[0] < BP_start->data[0]) {
    if (2 > BP_stop->size[1]) {
      i = 0;
      dimSize = 0;
    } else {
      i = 1;
      dimSize = BP_stop->size[1];
    }

    ixLead = b_BP_stop->size[0] * b_BP_stop->size[1];
    b_BP_stop->size[0] = 1;
    tmp1 = dimSize - i;
    b_BP_stop->size[1] = tmp1;
    emxEnsureCapacity_int32_T(b_BP_stop, ixLead);
    for (dimSize = 0; dimSize < tmp1; dimSize++) {
      b_BP_stop->data[dimSize] = BP_stop->data[i + dimSize];
    }

    i = BP_stop->size[0] * BP_stop->size[1];
    BP_stop->size[0] = 1;
    BP_stop->size[1] = b_BP_stop->size[1];
    emxEnsureCapacity_int32_T(BP_stop, i);
    tmp1 = b_BP_stop->size[0] * b_BP_stop->size[1];
    for (i = 0; i < tmp1; i++) {
      BP_stop->data[i] = b_BP_stop->data[i];
    }
  }

  emxFree_int32_T(&b_BP_stop);
  tmp1 = (int)fmin(BP_start->size[1], BP_stop->size[1]);
  i = footIndex->size[0] * footIndex->size[1];
  footIndex->size[0] = 1;
  footIndex->size[1] = tmp1;
  emxEnsureCapacity_real_T(footIndex, i);
  for (ixLead = 0; ixLead < tmp1; ixLead++) {
    footIndex->data[ixLead] = 0.0;
    i = BP_start->data[ixLead];
    dimSize = BP_stop->data[ixLead];
    if (i > dimSize) {
      i = -1;
      dimSize = -1;
    } else {
      i -= 2;
      dimSize--;
    }

    iyLead = dimSize - i;
    if (iyLead <= 2) {
      if (iyLead == 1) {
        work_data_idx_0 = 1;
      } else {
        d = waveformDDPlus->data[i + 2];
        if ((waveformDDPlus->data[i + 1] < d) || (rtIsNaN(waveformDDPlus->data[i
              + 1]) && (!rtIsNaN(d)))) {
          work_data_idx_0 = 2;
        } else {
          work_data_idx_0 = 1;
        }
      }
    } else {
      if (!rtIsNaN(waveformDDPlus->data[i + 1])) {
        work_data_idx_0 = 1;
      } else {
        work_data_idx_0 = 0;
        m = 2;
        exitg1 = false;
        while ((!exitg1) && (m <= iyLead)) {
          if (!rtIsNaN(waveformDDPlus->data[i + m])) {
            work_data_idx_0 = m;
            exitg1 = true;
          } else {
            m++;
          }
        }
      }

      if (work_data_idx_0 == 0) {
        work_data_idx_0 = 1;
      } else {
        ex = waveformDDPlus->data[i + work_data_idx_0];
        dimSize = work_data_idx_0 + 1;
        for (m = dimSize; m <= iyLead; m++) {
          d = waveformDDPlus->data[i + m];
          if (ex < d) {
            ex = d;
            work_data_idx_0 = m;
          }
        }
      }
    }

    footIndex->data[ixLead] = ((double)work_data_idx_0 + (double)BP_start->
      data[ixLead]) - 1.0;
  }

  emxFree_int32_T(&BP_stop);
  emxFree_int32_T(&BP_start);

  /*  Depending on the morphology of the signal, the foot index may be */
  /*  identified as the minimum of the waveform itself */
  /* { uncomment for local signal minimum identification */
  i = zoneWall_tmp->size[0] * zoneWall_tmp->size[1];
  zoneWall_tmp->size[0] = 1;
  zoneWall_tmp->size[1] = footIndex->size[1];
  emxEnsureCapacity_real_T(zoneWall_tmp, i);
  tmp1 = footIndex->size[0] * footIndex->size[1] - 1;
  for (i = 0; i <= tmp1; i++) {
    zoneWall_tmp->data[i] = footIndex->data[i];
  }

  FixIndex(zoneWall_tmp, bpwaveform, footIndex);

  /* } */
  emxFree_real_T(&zoneWall_tmp);
}

static void rollingWindow(const emxArray_real_T *vector, double winsize,
  emxArray_real_T *rwin)
{
  int i;
  int i1;
  int loop_ub;
  int b_i;
  int i2;

  /*  Rolling window functions */
  i = (int)winsize;
  i1 = rwin->size[0] * rwin->size[1];
  rwin->size[0] = i;
  rwin->size[1] = vector->size[1];
  emxEnsureCapacity_real_T(rwin, i1);
  loop_ub = i * vector->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    rwin->data[i1] = rtNaN;
  }

  for (b_i = 0; b_i < i; b_i++) {
    if (b_i + 1U > (unsigned int)rwin->size[1]) {
      i1 = 0;
      i2 = 0;
    } else {
      i1 = b_i;
      i2 = rwin->size[1];
    }

    loop_ub = i2 - i1;
    for (i2 = 0; i2 < loop_ub; i2++) {
      rwin->data[b_i + rwin->size[0] * (i1 + i2)] = vector->data[i2];
    }
  }
}

static void winmean(const emxArray_real_T *rwin, emxArray_real_T *res)
{
  unsigned int shape_idx_1;
  int i;
  int b_i;
  int i1;
  double y;
  int k;
  shape_idx_1 = (unsigned int)rwin->size[1];
  i = res->size[0] * res->size[1];
  res->size[0] = 1;
  res->size[1] = (int)shape_idx_1;
  emxEnsureCapacity_real_T(res, i);
  i = (int)shape_idx_1;
  for (b_i = 0; b_i < i; b_i++) {
    i1 = rwin->size[0];
    if (rwin->size[0] == 0) {
      y = 0.0;
    } else {
      y = rwin->data[rwin->size[0] * b_i];
      for (k = 2; k <= i1; k++) {
        y += rwin->data[(k + rwin->size[0] * b_i) - 1];
      }
    }

    res->data[b_i] = 1.5 * (y / (double)rwin->size[0]);
  }
}

static void winsum(const emxArray_real_T *rwin, emxArray_real_T *res)
{
  unsigned int shape_idx_1;
  int i;
  int b_i;
  int i1;
  double y;
  int k;
  shape_idx_1 = (unsigned int)rwin->size[1];
  i = res->size[0] * res->size[1];
  res->size[0] = 1;
  res->size[1] = (int)shape_idx_1;
  emxEnsureCapacity_real_T(res, i);
  i = (int)shape_idx_1;
  for (b_i = 0; b_i < i; b_i++) {
    i1 = rwin->size[0];
    if (rwin->size[0] == 0) {
      y = 0.0;
    } else {
      y = rwin->data[rwin->size[0] * b_i];
      for (k = 2; k <= i1; k++) {
        y += rwin->data[(k + rwin->size[0] * b_i) - 1];
      }
    }

    res->data[b_i] = y;
  }
}

void ppgannotation(const emxArray_real_T *inWaveform, emxArray_real_T *footIndex,
                   emxArray_real_T *systolicIndex, emxArray_real_T *notchIndex,
                   emxArray_real_T *dicroticIndex)
{
  emxArray_real_T *subIntegral;
  emxArray_real_T *firstNotNan;
  emxArray_real_T *y;
  double integwinsize;
  double threswinsize;
  double duration;
  int i;
  int loop_ub;
  emxArray_real_T *subThreshold;
  int nx;
  unsigned int outsize_idx_1;
  int k;
  emxArray_real_T *waveformD;
  int exitg1;
  emxArray_real_T *bpwaveform;
  int n;
  int overlap;
  emxArray_real_T *waveformDD;
  emxArray_real_T *waveformDDPlus;
  emxArray_real_T *BP_integral;
  emxArray_real_T *threshold;
  emxArray_real_T *integralWindow;
  emxArray_int32_T *r;
  emxArray_real_T *r1;
  emxArray_real_T *b_firstNotNan;
  double Start;
  double End;
  emxArray_boolean_T *b_BP_integral;
  int i1;
  emxArray_boolean_T *r2;
  emxArray_real_T *r3;
  emxArray_int32_T *r4;
  emxArray_int32_T *r5;
  emxInit_real_T(&subIntegral, 2);
  emxInit_real_T(&firstNotNan, 2);
  emxInit_real_T(&y, 1);

  /*  [ footIndex, systolicIndex, notchIndex, dicroticIndex, time, bpwaveform ] = BP_annotate( inWaveform, inFs, verbose, Units, isClean ) */
  /*  Implementation of a feature detection algorithm for arterial blood */
  /*  pressure in humans. The foot of the wave, systolic peak, dicrotic notch, */
  /*  and dicrotic peaks are identified. The blood pressure time series is */
  /*  always resampled at 200 Hz to allow standardisation. */
  /*  */
  /*  The technique was largely inspired by the derivatives and thresholds  */
  /*  described in Pan-Tompkins: */
  /*  Pan, Jiapu, and Willis J. Tompkins. "A real-time QRS detection algorithm."  */
  /*  IEEE transactions on biomedical engineering 3 (1985): 230-236. */
  /*  */
  /*  We also use criteria described by Sun, J. X., A. T. Reisner, and R. G. Mark. in */
  /*  "A signal abnormality index for arterial blood pressure waveforms." */
  /*  Computers in Cardiology, 2006. IEEE, 2006. */
  /*  These criteria have not been extensively tested by us. */
  /*  Authors */
  /*  Alexandre Laurin, PhD, ?cole Polytechnique, France, alexandre.laurin@inria.fr */
  /*  Jona Joachim, MD, H?pital Lariboisi?re, France, jona.joachim@inria.fr */
  /*  */
  /*  varargin handling */
  /*  setting global parameters     */
  Fs = 256.0;
  integwinsize = floor(Fs / 5.0);
  threswinsize = floor(Fs * 3.0);

  /*  resample the time-series to allow standardisation */
  /* BP_RESAMPLE Resample to 200 Hz */
  duration = (double)inWaveform->size[0] / 256.0;
  linspace(duration, inWaveform->size[0], firstNotNan);
  linspace(duration, Fs * duration, subIntegral);
  i = y->size[0];
  y->size[0] = inWaveform->size[0];
  emxEnsureCapacity_real_T(y, i);
  loop_ub = inWaveform->size[0];
  for (i = 0; i < loop_ub; i++) {
    y->data[i] = inWaveform->data[i];
  }

  emxInit_real_T(&subThreshold, 2);
  nx = firstNotNan->size[1] - 1;
  outsize_idx_1 = (unsigned int)subIntegral->size[1];
  i = subThreshold->size[0] * subThreshold->size[1];
  subThreshold->size[0] = 1;
  subThreshold->size[1] = (int)outsize_idx_1;
  emxEnsureCapacity_real_T(subThreshold, i);
  loop_ub = (int)outsize_idx_1;
  for (i = 0; i < loop_ub; i++) {
    subThreshold->data[i] = 0.0;
  }

  if (subIntegral->size[1] != 0) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= nx) {
        if (rtIsNaN(firstNotNan->data[k])) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        if (firstNotNan->data[1] < firstNotNan->data[0]) {
          i = (nx + 1) >> 1;
          for (n = 0; n < i; n++) {
            duration = firstNotNan->data[n];
            overlap = nx - n;
            firstNotNan->data[n] = firstNotNan->data[overlap];
            firstNotNan->data[overlap] = duration;
          }

          if ((inWaveform->size[0] != 0) && (inWaveform->size[0] > 1)) {
            n = inWaveform->size[0] - 1;
            nx = inWaveform->size[0] >> 1;
            for (k = 0; k < nx; k++) {
              duration = y->data[k];
              overlap = n - k;
              y->data[k] = y->data[overlap];
              y->data[overlap] = duration;
            }
          }
        }

        b_interp1SplineOrPCHIP(y, subIntegral, subThreshold, firstNotNan);
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  emxFree_real_T(&y);
  emxInit_real_T(&waveformD, 2);
  emxInit_real_T(&bpwaveform, 2);

  /*  filter */
  BP_lowpass(subThreshold, bpwaveform);

  /*  derivatives */
  diff(bpwaveform, firstNotNan);
  i = waveformD->size[0] * waveformD->size[1];
  waveformD->size[0] = 1;
  waveformD->size[1] = firstNotNan->size[1] + 1;
  emxEnsureCapacity_real_T(waveformD, i);
  loop_ub = firstNotNan->size[1];
  for (i = 0; i < loop_ub; i++) {
    waveformD->data[i] = firstNotNan->data[i];
  }

  waveformD->data[firstNotNan->size[1]] = rtNaN;
  diff(waveformD, firstNotNan);
  i = subIntegral->size[0] * subIntegral->size[1];
  subIntegral->size[0] = 1;
  subIntegral->size[1] = firstNotNan->size[1] + 1;
  emxEnsureCapacity_real_T(subIntegral, i);
  loop_ub = firstNotNan->size[1];
  for (i = 0; i < loop_ub; i++) {
    subIntegral->data[i] = firstNotNan->data[i];
  }

  emxInit_real_T(&waveformDD, 2);
  subIntegral->data[firstNotNan->size[1]] = rtNaN;
  BP_lowpass(subIntegral, waveformDD);

  /* Perform the switch for positive and negative first-derivative values */
  i = firstNotNan->size[0] * firstNotNan->size[1];
  firstNotNan->size[0] = 1;
  firstNotNan->size[1] = waveformDD->size[1];
  emxEnsureCapacity_real_T(firstNotNan, i);
  loop_ub = waveformDD->size[0] * waveformDD->size[1];
  for (i = 0; i < loop_ub; i++) {
    duration = waveformDD->data[i];
    firstNotNan->data[i] = duration * (double)((waveformD->data[i] > 0.0) &&
      (duration > 0.0));
  }

  emxInit_real_T(&waveformDDPlus, 2);
  i = waveformDDPlus->size[0] * waveformDDPlus->size[1];
  waveformDDPlus->size[0] = 1;
  waveformDDPlus->size[1] = firstNotNan->size[1];
  emxEnsureCapacity_real_T(waveformDDPlus, i);
  nx = firstNotNan->size[1];
  for (k = 0; k < nx; k++) {
    duration = firstNotNan->data[k];
    waveformDDPlus->data[k] = duration * duration;
  }

  /* Deal with very large data sets */
  emxInit_real_T(&BP_integral, 2);
  emxInit_real_T(&threshold, 2);
  emxInit_real_T(&integralWindow, 2);
  emxInit_int32_T(&r, 2);
  emxInit_real_T(&r1, 2);
  if (bpwaveform->size[1] > 300000) {
    /* fprintf('     Data set exceeds %i size limit, performing sub-windowing.',uint8(sizeLimit)) */
    outsize_idx_1 = (unsigned int)bpwaveform->size[1];
    i = BP_integral->size[0] * BP_integral->size[1];
    BP_integral->size[0] = 1;
    BP_integral->size[1] = (int)outsize_idx_1;
    emxEnsureCapacity_real_T(BP_integral, i);
    loop_ub = (int)outsize_idx_1;
    for (i = 0; i < loop_ub; i++) {
      BP_integral->data[i] = 0.0;
    }

    outsize_idx_1 = (unsigned int)bpwaveform->size[1];
    i = threshold->size[0] * threshold->size[1];
    threshold->size[0] = 1;
    threshold->size[1] = (int)outsize_idx_1;
    emxEnsureCapacity_real_T(threshold, i);
    loop_ub = (int)outsize_idx_1;
    for (i = 0; i < loop_ub; i++) {
      threshold->data[i] = 0.0;
    }

    nx = (int)ceil((double)bpwaveform->size[1] / 300000.0);
    overlap = (int)rt_roundd_snf(((double)nx * 300000.0 - (double)
      bpwaveform->size[1]) / ((double)nx - 1.0));
    emxInit_real_T(&b_firstNotNan, 2);
    for (k = 0; k < nx; k++) {
      printf(".");
      fflush(stdout);
      Start = ((((double)k + 1.0) - 1.0) * 300000.0 - (((double)k + 1.0) - 1.0) *
               (double)overlap) + 1.0;
      End = fmin((Start + 300000.0) - 1.0, bpwaveform->size[1]);
      if (Start > End) {
        i = 0;
        i1 = 0;
      } else {
        i = (int)Start - 1;
        i1 = (int)End;
      }

      n = subIntegral->size[0] * subIntegral->size[1];
      subIntegral->size[0] = 1;
      loop_ub = i1 - i;
      subIntegral->size[1] = loop_ub;
      emxEnsureCapacity_real_T(subIntegral, n);
      for (i1 = 0; i1 < loop_ub; i1++) {
        subIntegral->data[i1] = waveformDDPlus->data[i + i1];
      }

      rollingWindow(subIntegral, integwinsize, r1);
      i = integralWindow->size[0] * integralWindow->size[1];
      integralWindow->size[0] = r1->size[0];
      integralWindow->size[1] = r1->size[1];
      emxEnsureCapacity_real_T(integralWindow, i);
      loop_ub = r1->size[0] * r1->size[1];
      for (i = 0; i < loop_ub; i++) {
        integralWindow->data[i] = r1->data[i];
      }

      winsum(integralWindow, subIntegral);
      circshift(subIntegral, -floor(integwinsize / 2.0));

      /*  implementation of Sun et al. paper A Signal Abnormality Index for */
      /*  Arterial Blood Pressure Waveform */
      rollingWindow(subIntegral, threswinsize, r1);
      i = integralWindow->size[0] * integralWindow->size[1];
      integralWindow->size[0] = r1->size[0];
      integralWindow->size[1] = r1->size[1];
      emxEnsureCapacity_real_T(integralWindow, i);
      loop_ub = r1->size[0] * r1->size[1];
      for (i = 0; i < loop_ub; i++) {
        integralWindow->data[i] = r1->data[i];
      }

      winmean(integralWindow, subThreshold);
      if (overlap + 1 > subIntegral->size[1]) {
        i = 0;
        i1 = 0;
      } else {
        i = overlap;
        i1 = subIntegral->size[1];
      }

      duration = Start + (double)overlap;
      if (duration > End) {
        n = 1;
      } else {
        n = (int)duration;
      }

      loop_ub = i1 - i;
      for (i1 = 0; i1 < loop_ub; i1++) {
        BP_integral->data[(n + i1) - 1] = subIntegral->data[i + i1];
      }

      if (overlap + 1 > subThreshold->size[1]) {
        i = 0;
        i1 = 0;
      } else {
        i = overlap;
        i1 = subThreshold->size[1];
      }

      if (duration > End) {
        n = 1;
      } else {
        n = (int)duration;
      }

      loop_ub = i1 - i;
      for (i1 = 0; i1 < loop_ub; i1++) {
        threshold->data[(n + i1) - 1] = subThreshold->data[i + i1];
      }

      if (k + 1 > 1) {
        if (1 > overlap + 1) {
          loop_ub = -1;
        } else {
          loop_ub = overlap;
        }

        i = r->size[0] * r->size[1];
        r->size[0] = 1;
        i1 = overlap + 1;
        r->size[1] = i1;
        emxEnsureCapacity_int32_T(r, i);
        for (i = 0; i <= overlap; i++) {
          r->data[i] = (int)(Start + (double)i);
        }

        i = firstNotNan->size[0] * firstNotNan->size[1];
        firstNotNan->size[0] = 1;
        firstNotNan->size[1] = i1;
        emxEnsureCapacity_real_T(firstNotNan, i);
        for (i = 0; i <= overlap; i++) {
          firstNotNan->data[i] = BP_integral->data[(int)(Start + (double)i) - 1];
        }

        i = b_firstNotNan->size[0] * b_firstNotNan->size[1];
        b_firstNotNan->size[0] = 2;
        n = firstNotNan->size[1];
        b_firstNotNan->size[1] = firstNotNan->size[1];
        emxEnsureCapacity_real_T(b_firstNotNan, i);
        for (i = 0; i < n; i++) {
          b_firstNotNan->data[2 * i] = firstNotNan->data[i];
        }

        for (i = 0; i <= loop_ub; i++) {
          b_firstNotNan->data[2 * i + 1] = subIntegral->data[i];
        }

        mean(b_firstNotNan, subIntegral);
        loop_ub = subIntegral->size[0] * subIntegral->size[1];
        for (i = 0; i < loop_ub; i++) {
          BP_integral->data[r->data[i] - 1] = subIntegral->data[i];
        }

        if (1 > overlap + 1) {
          loop_ub = -1;
        } else {
          loop_ub = overlap;
        }

        i = r->size[0] * r->size[1];
        r->size[0] = 1;
        r->size[1] = i1;
        emxEnsureCapacity_int32_T(r, i);
        for (i = 0; i <= overlap; i++) {
          r->data[i] = (int)(Start + (double)i);
        }

        i = firstNotNan->size[0] * firstNotNan->size[1];
        firstNotNan->size[0] = 1;
        firstNotNan->size[1] = i1;
        emxEnsureCapacity_real_T(firstNotNan, i);
        for (i = 0; i <= overlap; i++) {
          firstNotNan->data[i] = threshold->data[(int)(Start + (double)i) - 1];
        }

        i = b_firstNotNan->size[0] * b_firstNotNan->size[1];
        b_firstNotNan->size[0] = 2;
        n = firstNotNan->size[1];
        b_firstNotNan->size[1] = firstNotNan->size[1];
        emxEnsureCapacity_real_T(b_firstNotNan, i);
        for (i = 0; i < n; i++) {
          b_firstNotNan->data[2 * i] = firstNotNan->data[i];
        }

        for (i = 0; i <= loop_ub; i++) {
          b_firstNotNan->data[2 * i + 1] = subThreshold->data[i];
        }

        mean(b_firstNotNan, subIntegral);
        loop_ub = subIntegral->size[0] * subIntegral->size[1];
        for (i = 0; i < loop_ub; i++) {
          threshold->data[r->data[i] - 1] = subIntegral->data[i];
        }
      } else {
        if (1 > overlap + 1) {
          loop_ub = -1;
        } else {
          loop_ub = overlap;
        }

        i = r->size[0] * r->size[1];
        r->size[0] = 1;
        i1 = overlap + 1;
        r->size[1] = i1;
        emxEnsureCapacity_int32_T(r, i);
        for (i = 0; i <= overlap; i++) {
          r->data[i] = (int)(Start + (double)i);
        }

        for (i = 0; i <= loop_ub; i++) {
          BP_integral->data[r->data[i] - 1] = subIntegral->data[i];
        }

        if (1 > overlap + 1) {
          loop_ub = -1;
        } else {
          loop_ub = overlap;
        }

        i = r->size[0] * r->size[1];
        r->size[0] = 1;
        r->size[1] = i1;
        emxEnsureCapacity_int32_T(r, i);
        for (i = 0; i <= overlap; i++) {
          r->data[i] = (int)(Start + (double)i);
        }

        for (i = 0; i <= loop_ub; i++) {
          threshold->data[r->data[i] - 1] = subThreshold->data[i];
        }
      }
    }

    emxFree_real_T(&b_firstNotNan);
    printf("\n");
    fflush(stdout);
  } else {
    /*  Moving sum to increase SNR */
    rollingWindow(waveformDDPlus, integwinsize, r1);
    i = integralWindow->size[0] * integralWindow->size[1];
    integralWindow->size[0] = r1->size[0];
    integralWindow->size[1] = r1->size[1];
    emxEnsureCapacity_real_T(integralWindow, i);
    loop_ub = r1->size[0] * r1->size[1];
    for (i = 0; i < loop_ub; i++) {
      integralWindow->data[i] = r1->data[i];
    }

    winsum(integralWindow, BP_integral);

    /*  implementation of Sun et al. paper A Signal Abnormality Index for */
    /*  Arterial Blood Pressure Waveform */
    /*  Center the integral */
    circshift(BP_integral, -floor(integwinsize / 2.0));
    rollingWindow(BP_integral, threswinsize, r1);
    i = integralWindow->size[0] * integralWindow->size[1];
    integralWindow->size[0] = r1->size[0];
    integralWindow->size[1] = r1->size[1];
    emxEnsureCapacity_real_T(integralWindow, i);
    loop_ub = r1->size[0] * r1->size[1];
    for (i = 0; i < loop_ub; i++) {
      integralWindow->data[i] = r1->data[i];
    }

    winmean(integralWindow, threshold);
  }

  emxFree_real_T(&r1);
  emxFree_real_T(&integralWindow);
  emxFree_real_T(&subThreshold);
  emxInit_boolean_T(&b_BP_integral, 2);

  /*  isClean forces the identification of indices before the window has */
  /*  had time to initiate */
  i = b_BP_integral->size[0] * b_BP_integral->size[1];
  b_BP_integral->size[0] = 1;
  b_BP_integral->size[1] = threshold->size[1];
  emxEnsureCapacity_boolean_T(b_BP_integral, i);
  loop_ub = threshold->size[0] * threshold->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_BP_integral->data[i] = rtIsNaN(threshold->data[i]);
  }

  emxInit_boolean_T(&r2, 2);
  i = r2->size[0] * r2->size[1];
  r2->size[0] = 1;
  r2->size[1] = b_BP_integral->size[1];
  emxEnsureCapacity_boolean_T(r2, i);
  loop_ub = b_BP_integral->size[0] * b_BP_integral->size[1];
  for (i = 0; i < loop_ub; i++) {
    r2->data[i] = !b_BP_integral->data[i];
  }

  eml_find(r2, r);
  i = firstNotNan->size[0] * firstNotNan->size[1];
  firstNotNan->size[0] = 1;
  firstNotNan->size[1] = r->size[1];
  emxEnsureCapacity_real_T(firstNotNan, i);
  loop_ub = r->size[0] * r->size[1];
  emxFree_boolean_T(&r2);
  for (i = 0; i < loop_ub; i++) {
    firstNotNan->data[i] = r->data[i];
  }

  emxFree_int32_T(&r);
  overlap = (int)firstNotNan->data[0];
  i = subIntegral->size[0] * subIntegral->size[1];
  subIntegral->size[0] = 1;
  loop_ub = (int)integwinsize;
  i1 = loop_ub + 1;
  subIntegral->size[1] = i1;
  emxEnsureCapacity_real_T(subIntegral, i);
  for (i = 0; i <= loop_ub; i++) {
    subIntegral->data[i] = threshold->data[(overlap + i) - 1];
  }

  if (i1 == 0) {
    duration = 0.0;
  } else {
    duration = threshold->data[overlap - 1];
    for (k = 2; k <= loop_ub + 1; k++) {
      duration += subIntegral->data[k - 1];
    }
  }

  duration /= (double)i1;
  i = b_BP_integral->size[0] * b_BP_integral->size[1];
  b_BP_integral->size[0] = 1;
  b_BP_integral->size[1] = threshold->size[1];
  emxEnsureCapacity_boolean_T(b_BP_integral, i);
  loop_ub = threshold->size[0] * threshold->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_BP_integral->data[i] = rtIsNaN(threshold->data[i]);
  }

  nx = b_BP_integral->size[1] - 1;
  n = 0;
  for (k = 0; k <= nx; k++) {
    if (b_BP_integral->data[k]) {
      n++;
    }
  }

  emxInit_real_T(&r3, 2);
  i = r3->size[0] * r3->size[1];
  r3->size[0] = 1;
  r3->size[1] = n;
  emxEnsureCapacity_real_T(r3, i);
  n = 0;
  for (k = 0; k <= nx; k++) {
    if (b_BP_integral->data[k]) {
      r3->data[n] = threshold->data[k];
      n++;
    }
  }

  nx = b_BP_integral->size[1] - 1;
  n = 0;
  for (k = 0; k <= nx; k++) {
    if (b_BP_integral->data[k]) {
      n++;
    }
  }

  emxInit_int32_T(&r4, 2);
  i = r4->size[0] * r4->size[1];
  r4->size[0] = 1;
  r4->size[1] = n;
  emxEnsureCapacity_int32_T(r4, i);
  n = 0;
  for (k = 0; k <= nx; k++) {
    if (b_BP_integral->data[k]) {
      r4->data[n] = k + 1;
      n++;
    }
  }

  loop_ub = r3->size[1] - 1;
  emxFree_real_T(&r3);
  for (i = 0; i <= loop_ub; i++) {
    threshold->data[r4->data[i] - 1] = duration;
  }

  emxFree_int32_T(&r4);

  /*  each zone of interest corresponds to a heart beat */
  i = b_BP_integral->size[0] * b_BP_integral->size[1];
  b_BP_integral->size[0] = 1;
  b_BP_integral->size[1] = BP_integral->size[1];
  emxEnsureCapacity_boolean_T(b_BP_integral, i);
  loop_ub = BP_integral->size[0] * BP_integral->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_BP_integral->data[i] = rtIsNaN(BP_integral->data[i]);
  }

  nx = b_BP_integral->size[1] - 1;
  n = 0;
  for (k = 0; k <= nx; k++) {
    if (b_BP_integral->data[k]) {
      n++;
    }
  }

  emxInit_int32_T(&r5, 2);
  i = r5->size[0] * r5->size[1];
  r5->size[0] = 1;
  r5->size[1] = n;
  emxEnsureCapacity_int32_T(r5, i);
  n = 0;
  for (k = 0; k <= nx; k++) {
    if (b_BP_integral->data[k]) {
      r5->data[n] = k + 1;
      n++;
    }
  }

  loop_ub = r5->size[0] * r5->size[1] - 1;
  for (i = 0; i <= loop_ub; i++) {
    BP_integral->data[r5->data[i] - 1] = 0.0;
  }

  emxFree_int32_T(&r5);

  /*  implementation of Sun et al. paper A Signal Abnormality Index for */
  /*  Arterial Blood Pressure Waveform */
  i = b_BP_integral->size[0] * b_BP_integral->size[1];
  b_BP_integral->size[0] = 1;
  b_BP_integral->size[1] = BP_integral->size[1];
  emxEnsureCapacity_boolean_T(b_BP_integral, i);
  loop_ub = BP_integral->size[0] * BP_integral->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_BP_integral->data[i] = (BP_integral->data[i] > threshold->data[i]);
  }

  emxFree_real_T(&threshold);
  emxFree_real_T(&BP_integral);
  getFootIndex(bpwaveform, waveformDDPlus, b_BP_integral, footIndex);
  duration = floor(integwinsize / 2.0);
  i = subIntegral->size[0] * subIntegral->size[1];
  subIntegral->size[0] = 1;
  subIntegral->size[1] = footIndex->size[1];
  emxEnsureCapacity_real_T(subIntegral, i);
  loop_ub = footIndex->size[0] * footIndex->size[1];
  emxFree_boolean_T(&b_BP_integral);
  emxFree_real_T(&waveformDDPlus);
  for (i = 0; i < loop_ub; i++) {
    subIntegral->data[i] = footIndex->data[i] + duration;
  }

  b_FixIndex(subIntegral, bpwaveform, floor(integwinsize / 2.0), systolicIndex);

  /*  implementation of Sun et al. paper A Signal Abnormality Index for */
  /*  Arterial Blood Pressure Waveform */
  i = firstNotNan->size[0] * firstNotNan->size[1];
  firstNotNan->size[0] = 1;
  firstNotNan->size[1] = systolicIndex->size[1];
  emxEnsureCapacity_real_T(firstNotNan, i);
  loop_ub = systolicIndex->size[0] * systolicIndex->size[1] - 1;
  emxFree_real_T(&subIntegral);
  for (i = 0; i <= loop_ub; i++) {
    firstNotNan->data[i] = systolicIndex->data[i];
  }

  getDicroticIndex(waveformDD, waveformD, bpwaveform, footIndex, firstNotNan,
                   dicroticIndex, notchIndex);
  i = footIndex->size[0] * footIndex->size[1];
  if (1 > notchIndex->size[1]) {
    footIndex->size[1] = 0;
  } else {
    footIndex->size[1] = notchIndex->size[1];
  }

  emxEnsureCapacity_real_T(footIndex, i);
  i = systolicIndex->size[0] * systolicIndex->size[1];
  if (1 > notchIndex->size[1]) {
    systolicIndex->size[1] = 0;
  } else {
    systolicIndex->size[1] = notchIndex->size[1];
  }

  emxEnsureCapacity_real_T(systolicIndex, i);

  /*      if verbose */
  /*          figure; */
  /*          Colors = get(gca, 'ColorOrder'); */
  /*          axs(1) = subplot(2, 1, 1); */
  /*          hold on; */
  /*          plot(time, bpwaveform); */
  /*          plot(origtime, inWaveform); */
  /*          plot(time(footIndex), bpwaveform(footIndex),         '<', 'color', Colors(4,:), 'markerfacecolor', Colors(4,:)) */
  /*          plot(time(systolicIndex), bpwaveform(systolicIndex), '^', 'color', Colors(5,:), 'markerfacecolor', Colors(5,:)) */
  /*          plot(time(notchIndex), bpwaveform(notchIndex),       '^', 'color', Colors(6,:), 'markerfacecolor', Colors(6,:)) */
  /*          plot(time(dicroticIndex), bpwaveform(dicroticIndex), '^', 'color', Colors(7,:), 'markerfacecolor', Colors(7,:)) */
  /*          legend({'Filtered','Waveform','Foot','Systole', 'Notch', 'Dicrotic Peak'},'box','off') */
  /*          ylabel('arterial pressure') */
  /*   */
  /*          axs(2) = subplot(2, 1, 2); */
  /*          hold on; */
  /*          plot(time, waveformDD); */
  /*          plot(time, BP_integral); */
  /*          plot(time, threshold); */
  /*          plot(time, zoneOfInterest .* .1); */
  /*          legend({'2nd Derivative','Integral', 'Threshold', 'ZOI'}, 'box','off'); */
  /*          xlabel('time (s)') */
  /*          linkaxes(axs, 'x') */
  /*      end */
  emxFree_real_T(&bpwaveform);
  emxFree_real_T(&waveformD);
  emxFree_real_T(&waveformDD);
  emxFree_real_T(&firstNotNan);
}

/* End of code generation (ppgannotation.c) */
