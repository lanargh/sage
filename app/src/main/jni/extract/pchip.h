/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * pchip.h
 *
 * Code generation for function 'pchip'
 *
 */

#ifndef PCHIP_H
#define PCHIP_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern double exteriorSlope(double d1, double d2, double h1, double h2);

#endif

/* End of code generation (pchip.h) */
