/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * extract_initialize.h
 *
 * Code generation for function 'extract_initialize'
 *
 */

#ifndef EXTRACT_INITIALIZE_H
#define EXTRACT_INITIALIZE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern void extract_initialize(void);

#endif

/* End of code generation (extract_initialize.h) */
