/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * bsearch.h
 *
 * Code generation for function 'bsearch'
 *
 */

#ifndef BSEARCH_H
#define BSEARCH_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern int b_bsearch(const emxArray_real_T *x, double xi);

#endif

/* End of code generation (bsearch.h) */
