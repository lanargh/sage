/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * extract_data.h
 *
 * Code generation for function 'extract_data'
 *
 */

#ifndef EXTRACT_DATA_H
#define EXTRACT_DATA_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Variable Declarations */
extern double Fs;
extern omp_nest_lock_t emlrtNestLockGlobal;
extern boolean_T isInitialized_extract;

#endif

/* End of code generation (extract_data.h) */
