/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * extract_initialize.c
 *
 * Code generation for function 'extract_initialize'
 *
 */

/* Include files */
#include "extract_initialize.h"
#include "extract.h"
#include "extract_data.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void extract_initialize(void)
{
  rt_InitInfAndNaN();
  omp_init_nest_lock(&emlrtNestLockGlobal);
  Fs = 256.0;
  isInitialized_extract = true;
}

/* End of code generation (extract_initialize.c) */
