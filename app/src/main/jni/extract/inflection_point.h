/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * inflection_point.h
 *
 * Code generation for function 'inflection_point'
 *
 */

#ifndef INFLECTION_POINT_H
#define INFLECTION_POINT_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern double inflection_point(const emxArray_real_T *s, double min_value,
  double max_value);

#endif

/* End of code generation (inflection_point.h) */
