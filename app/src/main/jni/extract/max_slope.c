/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * max_slope.c
 *
 * Code generation for function 'max_slope'
 *
 */

/* Include files */
#include "max_slope.h"
#include "extract.h"
#include "extract_emxutil.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void max_slope(const emxArray_real_T *s, double min_value, double max_value,
               emxArray_real_T *op)
{
  int i;
  int ixLead;
  int iyLead;
  int dimSize_tmp;
  emxArray_real_T *b_y1;
  int idx;
  double tmp1;
  boolean_T exitg1;
  emxArray_boolean_T *x;
  double work_data_idx_0;
  double d;
  emxArray_int32_T *ii;
  if (min_value > max_value) {
    i = 1;
    ixLead = 0;
  } else {
    i = (int)min_value;
    ixLead = (int)max_value;
  }

  iyLead = ixLead - i;
  dimSize_tmp = iyLead + 1;
  emxInit_real_T(&b_y1, 1);
  if (dimSize_tmp == 0) {
    b_y1->size[0] = 0;
  } else {
    if (iyLead < 1) {
      ixLead = iyLead;
    } else {
      ixLead = 1;
    }

    if (ixLead < 1) {
      b_y1->size[0] = 0;
    } else {
      ixLead = b_y1->size[0];
      b_y1->size[0] = iyLead;
      emxEnsureCapacity_real_T(b_y1, ixLead);
      if (iyLead != 0) {
        ixLead = 0;
        iyLead = 0;
        work_data_idx_0 = s->data[i - 1];
        for (idx = 2; idx <= dimSize_tmp; idx++) {
          tmp1 = s->data[i + ixLead];
          d = tmp1;
          tmp1 -= work_data_idx_0;
          work_data_idx_0 = d;
          ixLead++;
          b_y1->data[iyLead] = tmp1;
          iyLead++;
        }
      }
    }
  }

  ixLead = b_y1->size[0];
  if (b_y1->size[0] <= 2) {
    if (b_y1->size[0] == 1) {
      tmp1 = b_y1->data[0];
    } else if ((b_y1->data[0] < b_y1->data[1]) || (rtIsNaN(b_y1->data[0]) &&
                (!rtIsNaN(b_y1->data[1])))) {
      tmp1 = b_y1->data[1];
    } else {
      tmp1 = b_y1->data[0];
    }
  } else {
    if (!rtIsNaN(b_y1->data[0])) {
      idx = 1;
    } else {
      idx = 0;
      iyLead = 2;
      exitg1 = false;
      while ((!exitg1) && (iyLead <= b_y1->size[0])) {
        if (!rtIsNaN(b_y1->data[iyLead - 1])) {
          idx = iyLead;
          exitg1 = true;
        } else {
          iyLead++;
        }
      }
    }

    if (idx == 0) {
      tmp1 = b_y1->data[0];
    } else {
      tmp1 = b_y1->data[idx - 1];
      i = idx + 1;
      for (iyLead = i; iyLead <= ixLead; iyLead++) {
        d = b_y1->data[iyLead - 1];
        if (tmp1 < d) {
          tmp1 = d;
        }
      }
    }
  }

  emxInit_boolean_T(&x, 1);

  /*  max_slope: */
  i = x->size[0];
  x->size[0] = b_y1->size[0];
  emxEnsureCapacity_boolean_T(x, i);
  ixLead = b_y1->size[0];
  for (i = 0; i < ixLead; i++) {
    x->data[i] = (b_y1->data[i] == tmp1);
  }

  emxFree_real_T(&b_y1);
  emxInit_int32_T(&ii, 1);
  iyLead = x->size[0];
  idx = 0;
  i = ii->size[0];
  ii->size[0] = x->size[0];
  emxEnsureCapacity_int32_T(ii, i);
  ixLead = 0;
  exitg1 = false;
  while ((!exitg1) && (ixLead <= iyLead - 1)) {
    if (x->data[ixLead]) {
      idx++;
      ii->data[idx - 1] = ixLead + 1;
      if (idx >= iyLead) {
        exitg1 = true;
      } else {
        ixLead++;
      }
    } else {
      ixLead++;
    }
  }

  if (x->size[0] == 1) {
    if (idx == 0) {
      ii->size[0] = 0;
    }
  } else {
    i = ii->size[0];
    if (1 > idx) {
      ii->size[0] = 0;
    } else {
      ii->size[0] = idx;
    }

    emxEnsureCapacity_int32_T(ii, i);
  }

  emxFree_boolean_T(&x);
  i = op->size[0];
  op->size[0] = ii->size[0];
  emxEnsureCapacity_real_T(op, i);
  ixLead = ii->size[0];
  for (i = 0; i < ixLead; i++) {
    op->data[i] = min_value + (double)ii->data[i];
  }

  emxFree_int32_T(&ii);
}

/* End of code generation (max_slope.c) */
