/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * spline.c
 *
 * Code generation for function 'spline'
 *
 */

/* Include files */
#include "spline.h"
#include "extract.h"
#include "extract_emxutil.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */
void spline(const emxArray_real_T *x, const emxArray_real_T *y, emxArray_real_T *
            output_breaks, emxArray_real_T *output_coefs)
{
  boolean_T has_endslopes;
  emxArray_real_T *s;
  emxArray_real_T *dvdf;
  emxArray_real_T *dx;
  emxArray_real_T *md;
  emxArray_real_T *t1_coefs;
  int nxm1;
  double szdvdf_idx_1;
  int cpage;
  int szs_idx_1;
  int yoffset;
  double dnnm2;
  double c_data[4];
  int i;
  double r;
  double pp1_coefs[4];
  double dzzdx;
  has_endslopes = (y->size[1] == x->size[0] + 2);
  emxInit_real_T(&s, 2);
  emxInit_real_T(&dvdf, 2);
  emxInit_real_T(&dx, 1);
  emxInit_real_T(&md, 1);
  emxInit_real_T(&t1_coefs, 2);
  if ((x->size[0] <= 2) || ((x->size[0] <= 3) && (!has_endslopes))) {
    has_endslopes = (y->size[1] == x->size[0] + 2);
    if (x->size[0] <= 2) {
      if (has_endslopes) {
        cpage = 4;
      } else {
        cpage = 2;
      }

      if (has_endslopes) {
        dnnm2 = y->data[0];
        if (0 <= x->size[0] - 2) {
          r = x->data[1] - x->data[0];
          szdvdf_idx_1 = (y->data[2] - y->data[1]) / r;
          dzzdx = (szdvdf_idx_1 - dnnm2) / r;
          szdvdf_idx_1 = (y->data[y->size[1] - 1] - szdvdf_idx_1) / r;
          pp1_coefs[0] = (szdvdf_idx_1 - dzzdx) / r;
          pp1_coefs[1] = 2.0 * dzzdx - szdvdf_idx_1;
          pp1_coefs[2] = dnnm2;
          pp1_coefs[3] = y->data[1];
        }

        if (0 <= cpage - 1) {
          memcpy(&c_data[0], &pp1_coefs[0], cpage * sizeof(double));
        }
      } else {
        c_data[0] = (y->data[1] - y->data[0]) / (x->data[1] - x->data[0]);
        c_data[1] = y->data[0];
      }

      i = output_breaks->size[0] * output_breaks->size[1];
      output_breaks->size[0] = 1;
      output_breaks->size[1] = x->size[0];
      emxEnsureCapacity_real_T(output_breaks, i);
      szs_idx_1 = x->size[0];
      for (i = 0; i < szs_idx_1; i++) {
        output_breaks->data[i] = x->data[i];
      }
    } else {
      cpage = 3;
      szdvdf_idx_1 = x->data[1] - x->data[0];
      dnnm2 = (y->data[1] - y->data[0]) / szdvdf_idx_1;
      c_data[0] = ((y->data[2] - y->data[1]) / (x->data[2] - x->data[1]) - dnnm2)
        / (x->data[2] - x->data[0]);
      c_data[1] = dnnm2 - c_data[0] * szdvdf_idx_1;
      c_data[2] = y->data[0];
      i = output_breaks->size[0] * output_breaks->size[1];
      output_breaks->size[0] = 1;
      output_breaks->size[1] = 2;
      emxEnsureCapacity_real_T(output_breaks, i);
      output_breaks->data[0] = x->data[0];
      output_breaks->data[1] = x->data[2];
    }

    i = output_coefs->size[0] * output_coefs->size[1];
    output_coefs->size[0] = 1;
    output_coefs->size[1] = cpage;
    emxEnsureCapacity_real_T(output_coefs, i);
    for (i = 0; i < cpage; i++) {
      output_coefs->data[i] = c_data[i];
    }
  } else {
    nxm1 = x->size[0] - 1;
    if (has_endslopes) {
      szdvdf_idx_1 = (double)y->size[1] - 3.0;
      szs_idx_1 = y->size[1] - 2;
      yoffset = 1;
    } else {
      szdvdf_idx_1 = (double)y->size[1] - 1.0;
      szs_idx_1 = y->size[1];
      yoffset = 0;
    }

    i = s->size[0] * s->size[1];
    s->size[0] = 1;
    s->size[1] = szs_idx_1;
    emxEnsureCapacity_real_T(s, i);
    i = dvdf->size[0] * dvdf->size[1];
    dvdf->size[0] = 1;
    dvdf->size[1] = (int)szdvdf_idx_1;
    emxEnsureCapacity_real_T(dvdf, i);
    i = dx->size[0];
    dx->size[0] = x->size[0] - 1;
    emxEnsureCapacity_real_T(dx, i);
    for (cpage = 0; cpage < nxm1; cpage++) {
      szdvdf_idx_1 = x->data[cpage + 1] - x->data[cpage];
      dx->data[cpage] = szdvdf_idx_1;
      szs_idx_1 = yoffset + cpage;
      dvdf->data[cpage] = (y->data[szs_idx_1 + 1] - y->data[szs_idx_1]) /
        szdvdf_idx_1;
    }

    for (cpage = 2; cpage <= nxm1; cpage++) {
      s->data[cpage - 1] = 3.0 * (dx->data[cpage - 1] * dvdf->data[cpage - 2] +
        dx->data[cpage - 2] * dvdf->data[cpage - 1]);
    }

    if (has_endslopes) {
      szdvdf_idx_1 = 0.0;
      dnnm2 = 0.0;
      s->data[0] = dx->data[1] * y->data[0];
      s->data[x->size[0] - 1] = dx->data[x->size[0] - 3] * y->data[x->size[0] +
        1];
    } else {
      szdvdf_idx_1 = x->data[2] - x->data[0];
      dnnm2 = x->data[x->size[0] - 1] - x->data[x->size[0] - 3];
      s->data[0] = ((dx->data[0] + 2.0 * szdvdf_idx_1) * dx->data[1] *
                    dvdf->data[0] + dx->data[0] * dx->data[0] * dvdf->data[1]) /
        szdvdf_idx_1;
      s->data[x->size[0] - 1] = ((dx->data[x->size[0] - 2] + 2.0 * dnnm2) *
        dx->data[x->size[0] - 3] * dvdf->data[x->size[0] - 2] + dx->data[x->
        size[0] - 2] * dx->data[x->size[0] - 2] * dvdf->data[x->size[0] - 3]) /
        dnnm2;
    }

    i = md->size[0];
    md->size[0] = x->size[0];
    emxEnsureCapacity_real_T(md, i);
    md->data[0] = dx->data[1];
    md->data[x->size[0] - 1] = dx->data[x->size[0] - 3];
    for (cpage = 2; cpage <= nxm1; cpage++) {
      md->data[cpage - 1] = 2.0 * (dx->data[cpage - 1] + dx->data[cpage - 2]);
    }

    r = dx->data[1] / md->data[0];
    md->data[1] -= r * szdvdf_idx_1;
    s->data[1] -= r * s->data[0];
    for (cpage = 3; cpage <= nxm1; cpage++) {
      r = dx->data[cpage - 1] / md->data[cpage - 2];
      md->data[cpage - 1] -= r * dx->data[cpage - 3];
      s->data[cpage - 1] -= r * s->data[cpage - 2];
    }

    r = dnnm2 / md->data[x->size[0] - 2];
    md->data[x->size[0] - 1] -= r * dx->data[x->size[0] - 3];
    s->data[x->size[0] - 1] -= r * s->data[x->size[0] - 2];
    s->data[x->size[0] - 1] /= md->data[x->size[0] - 1];
    for (cpage = nxm1; cpage >= 2; cpage--) {
      s->data[cpage - 1] = (s->data[cpage - 1] - dx->data[cpage - 2] * s->
                            data[cpage]) / md->data[cpage - 1];
    }

    s->data[0] = (s->data[0] - szdvdf_idx_1 * s->data[1]) / md->data[0];
    nxm1 = x->size[0];
    cpage = s->size[1] - 1;
    i = t1_coefs->size[0] * t1_coefs->size[1];
    t1_coefs->size[0] = s->size[1] - 1;
    t1_coefs->size[1] = 4;
    emxEnsureCapacity_real_T(t1_coefs, i);
    for (szs_idx_1 = 0; szs_idx_1 <= nxm1 - 2; szs_idx_1++) {
      szdvdf_idx_1 = dvdf->data[szs_idx_1];
      dnnm2 = s->data[szs_idx_1];
      dzzdx = (szdvdf_idx_1 - dnnm2) / dx->data[szs_idx_1];
      szdvdf_idx_1 = (s->data[szs_idx_1 + 1] - szdvdf_idx_1) / dx->
        data[szs_idx_1];
      t1_coefs->data[szs_idx_1] = (szdvdf_idx_1 - dzzdx) / dx->data[szs_idx_1];
      t1_coefs->data[cpage + szs_idx_1] = 2.0 * dzzdx - szdvdf_idx_1;
      t1_coefs->data[(cpage << 1) + szs_idx_1] = dnnm2;
      t1_coefs->data[3 * cpage + szs_idx_1] = y->data[yoffset + szs_idx_1];
    }

    i = output_breaks->size[0] * output_breaks->size[1];
    output_breaks->size[0] = 1;
    output_breaks->size[1] = x->size[0];
    emxEnsureCapacity_real_T(output_breaks, i);
    szs_idx_1 = x->size[0];
    for (i = 0; i < szs_idx_1; i++) {
      output_breaks->data[i] = x->data[i];
    }

    i = output_coefs->size[0] * output_coefs->size[1];
    output_coefs->size[0] = t1_coefs->size[0];
    output_coefs->size[1] = 4;
    emxEnsureCapacity_real_T(output_coefs, i);
    szs_idx_1 = t1_coefs->size[0] * t1_coefs->size[1];
    for (i = 0; i < szs_idx_1; i++) {
      output_coefs->data[i] = t1_coefs->data[i];
    }
  }

  emxFree_real_T(&t1_coefs);
  emxFree_real_T(&md);
  emxFree_real_T(&dx);
  emxFree_real_T(&dvdf);
  emxFree_real_T(&s);
}

/* End of code generation (spline.c) */
