/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * extract_data.c
 *
 * Code generation for function 'extract_data'
 *
 */

/* Include files */
#include "extract_data.h"
#include "extract.h"
#include "rt_nonfinite.h"

/* Variable Definitions */
double Fs;
omp_nest_lock_t emlrtNestLockGlobal;
boolean_T isInitialized_extract = false;

/* End of code generation (extract_data.c) */
