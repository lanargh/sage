/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ecgwindowedfilter.h
 *
 * Code generation for function 'ecgwindowedfilter'
 *
 */

#ifndef ECGWINDOWEDFILTER_H
#define ECGWINDOWEDFILTER_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern void ecgwindowedfilter(const emxArray_real_T *Original, emxArray_real_T
  *Filtered);

#endif

/* End of code generation (ecgwindowedfilter.h) */
