/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ecgwindowedfilter.c
 *
 * Code generation for function 'ecgwindowedfilter'
 *
 */

/* Include files */
#include "ecgwindowedfilter.h"
#include "extract.h"
#include "extract_emxutil.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void ecgwindowedfilter(const emxArray_real_T *Original, emxArray_real_T
  *Filtered)
{
  double FrontIterator;
  double WinMaxPos;
  double WinMax;
  unsigned int unnamed_idx_1;
  int i;
  int loop_ub;
  int LengthCounter;
  double d;
  double OutputIterator;
  double b_FrontIterator;
  double WinPos;
  int i1;
  int WinIterator;

  /*   ECG PROCESSING DEMONSTRATION - R-PEAKS DETECTION */
  /*    The package downloaded from http://www.librow.com */
  /*               To contact the author of the sample write to Sergey Chernenko: */
  /*               S.Chernenko@librow.com */
  /*  */
  /* initialising variables */
  FrontIterator = 1.0;
  WinMaxPos = 73.0;
  WinMax = Original->data[0];
  unnamed_idx_1 = (unsigned int)Original->size[1];
  i = Filtered->size[0] * Filtered->size[1];
  Filtered->size[0] = 1;
  Filtered->size[1] = (int)unnamed_idx_1;
  emxEnsureCapacity_real_T(Filtered, i);
  loop_ub = (int)unnamed_idx_1;
  for (i = 0; i < loop_ub; i++) {
    Filtered->data[i] = 0.0;
  }

  /*  Finding the postion of the largest value in window */
  for (LengthCounter = 0; LengthCounter < 73; LengthCounter++) {
    d = Original->data[(int)(FrontIterator + 1.0) - 1];
    if (d > WinMax) {
      WinMax = d;
      WinMaxPos = (double)LengthCounter + 74.0;
    }

    FrontIterator++;
  }

  /*  if the first point is the highest, set ouput 1 */
  if (WinMaxPos == 73.0) {
    Filtered->data[0] = WinMax;
  } else {
    Filtered->data[0] = 0.0;
  }

  OutputIterator = 1.0;

  /*  search next half of signal */
  for (LengthCounter = 0; LengthCounter < 73; LengthCounter++) {
    d = Original->data[(int)(FrontIterator + 1.0) - 1];
    if (d > WinMax) {
      WinMax = d;
      WinMaxPos = 146.0;
    } else {
      WinMaxPos--;
    }

    if (WinMaxPos == 73.0) {
      Filtered->data[(int)(OutputIterator + 1.0) - 1] = WinMax;
    } else {
      Filtered->data[(int)(OutputIterator + 1.0) - 1] = 0.0;
    }

    FrontIterator++;
    OutputIterator++;
  }

  b_FrontIterator = FrontIterator;
  i = (int)(((double)Original->size[1] - 1.0) + (1.0 - FrontIterator));
  for (loop_ub = 0; loop_ub < i; loop_ub++) {
    FrontIterator = b_FrontIterator + (double)loop_ub;
    d = Original->data[(int)(FrontIterator + 1.0) - 1];
    if (d > WinMax) {
      WinMax = d;
      WinMaxPos = 146.0;
    } else {
      WinMaxPos--;
      if (WinMaxPos < 0.0) {
        WinMax = Original->data[(int)((FrontIterator - 146.0) + 1.0) - 1];
        WinMaxPos = 0.0;
        WinPos = 0.0;
        i1 = (int)(FrontIterator + (1.0 - (FrontIterator - 146.0)));
        for (WinIterator = 0; WinIterator < i1; WinIterator++) {
          LengthCounter = (int)(((FrontIterator - 146.0) + (double)WinIterator)
                                + 1.0) - 1;
          if (Original->data[LengthCounter] > WinMax) {
            WinMax = Original->data[LengthCounter];
            WinMaxPos = WinPos;
          }

          WinPos++;
        }
      }
    }

    if (WinMaxPos == 73.0) {
      Filtered->data[(int)(OutputIterator + 1.0) - 1] = WinMax;
    } else {
      Filtered->data[(int)(OutputIterator + 1.0) - 1] = 0.0;
    }

    OutputIterator++;
  }

  WinMaxPos--;
  for (LengthCounter = 0; LengthCounter < 73; LengthCounter++) {
    if (WinMaxPos < 0.0) {
      loop_ub = (Original->size[1] + LengthCounter) - 146;
      WinMax = Original->data[loop_ub];
      WinMaxPos = 0.0;
      WinPos = 1.0;
      i = loop_ub + 1;
      i1 = Original->size[1] - loop_ub;
      for (WinIterator = 0; WinIterator <= i1 - 2; WinIterator++) {
        d = Original->data[(int)(((double)i + (double)WinIterator) + 1.0) - 1];
        if (d > WinMax) {
          WinMax = d;
          WinMaxPos = WinPos;
        }

        WinPos++;
      }
    }

    if (WinMaxPos == 73.0) {
      Filtered->data[(int)(OutputIterator + 1.0) - 1] = WinMax;
    } else {
      Filtered->data[(int)(OutputIterator + 1.0) - 1] = 0.0;
    }

    WinMaxPos--;
    OutputIterator++;
  }
}

/* End of code generation (ecgwindowedfilter.c) */
