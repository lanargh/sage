/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ppgannotation.h
 *
 * Code generation for function 'ppgannotation'
 *
 */

#ifndef PPGANNOTATION_H
#define PPGANNOTATION_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern void ppgannotation(const emxArray_real_T *inWaveform, emxArray_real_T
  *footIndex, emxArray_real_T *systolicIndex, emxArray_real_T *notchIndex,
  emxArray_real_T *dicroticIndex);

#endif

/* End of code generation (ppgannotation.h) */
