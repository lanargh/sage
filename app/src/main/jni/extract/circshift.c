/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * circshift.c
 *
 * Code generation for function 'circshift'
 *
 */

/* Include files */
#include "circshift.h"
#include "extract.h"
#include "extract_emxutil.h"
#include "rt_nonfinite.h"
#include <math.h>

/* Function Declarations */
static int div_s32_sat(int numerator, int denominator);

/* Function Definitions */
static int div_s32_sat(int numerator, int denominator)
{
  int quotient;
  boolean_T quotientNeedsNegation;
  unsigned int b_numerator;
  unsigned int b_denominator;
  if (denominator == 0) {
    if (numerator >= 0) {
      quotient = MAX_int32_T;
    } else {
      quotient = MIN_int32_T;
    }
  } else {
    quotientNeedsNegation = ((numerator < 0) != (denominator < 0));
    if (numerator < 0) {
      b_numerator = ~(unsigned int)numerator + 1U;
    } else {
      b_numerator = (unsigned int)numerator;
    }

    if (denominator < 0) {
      b_denominator = ~(unsigned int)denominator + 1U;
    } else {
      b_denominator = (unsigned int)denominator;
    }

    b_numerator /= b_denominator;
    if ((!quotientNeedsNegation) && (b_numerator >= 2147483647U)) {
      quotient = MAX_int32_T;
    } else if (quotientNeedsNegation && (b_numerator > 2147483647U)) {
      quotient = MIN_int32_T;
    } else if (quotientNeedsNegation) {
      quotient = -(int)b_numerator;
    } else {
      quotient = (int)b_numerator;
    }
  }

  return quotient;
}

void circshift(emxArray_real_T *a, double p)
{
  emxArray_real_T *buffer;
  int ns;
  boolean_T shiftright;
  int i;
  int loop_ub;
  int k;
  emxInit_real_T(&buffer, 2);
  if ((a->size[1] != 0) && (a->size[1] != 1)) {
    if (p < 0.0) {
      ns = (int)-p;
      shiftright = false;
    } else {
      ns = (int)p;
      shiftright = true;
    }

    if (ns > a->size[1]) {
      ns -= div_s32_sat(ns, a->size[1]) * a->size[1];
    }

    if (ns > (a->size[1] >> 1)) {
      ns = a->size[1] - ns;
      shiftright = !shiftright;
    }

    i = buffer->size[0] * buffer->size[1];
    buffer->size[0] = 1;
    loop_ub = (int)floor((double)a->size[1] / 2.0);
    buffer->size[1] = loop_ub;
    emxEnsureCapacity_real_T(buffer, i);
    for (i = 0; i < loop_ub; i++) {
      buffer->data[i] = 0.0;
    }

    loop_ub = a->size[1];
    if ((a->size[1] > 1) && (ns > 0)) {
      if (shiftright) {
        for (k = 0; k < ns; k++) {
          buffer->data[k] = a->data[(k + loop_ub) - ns];
        }

        i = ns + 1;
        for (k = loop_ub; k >= i; k--) {
          a->data[k - 1] = a->data[(k - ns) - 1];
        }

        for (k = 0; k < ns; k++) {
          a->data[k] = buffer->data[k];
        }
      } else {
        for (k = 0; k < ns; k++) {
          buffer->data[k] = a->data[k];
        }

        i = a->size[1] - ns;
        for (k = 0; k < i; k++) {
          a->data[k] = a->data[k + ns];
        }

        for (k = 0; k < ns; k++) {
          a->data[(k + loop_ub) - ns] = buffer->data[k];
        }
      }
    }
  }

  emxFree_real_T(&buffer);
}

/* End of code generation (circshift.c) */
