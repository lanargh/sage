/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * envelope.h
 *
 * Code generation for function 'envelope'
 *
 */

#ifndef ENVELOPE_H
#define ENVELOPE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern void envelope(const emxArray_real_T *x, emxArray_real_T *upperEnv,
                     emxArray_real_T *lowerEnv);

#endif

/* End of code generation (envelope.h) */
