/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * fft.c
 *
 * Code generation for function 'fft'
 *
 */

/* Include files */
#include "fft.h"
#include "extract.h"
#include "extract_emxutil.h"
#include "fft1.h"
#include "rt_nonfinite.h"
#include <math.h>

/* Function Definitions */
void fft(const emxArray_real_T *x, emxArray_creal_T *y)
{
  emxArray_creal_T *b_y;
  int n;
  emxArray_real_T *costab1q;
  emxArray_real_T *costab;
  emxArray_real_T *sintab;
  emxArray_real_T *sintabinv;
  emxArray_creal_T *wwc;
  emxArray_creal_T *fv;
  emxArray_creal_T *b_fv;
  boolean_T useRadix2;
  int i;
  int N2blue;
  int nd2;
  double twid_re;
  int j;
  int ihi;
  int c_y;
  int nInt2;
  int nRowsD2;
  int nRowsD4;
  int idx;
  int rt;
  double nt_im;
  double nt_re;
  double twid_im;
  emxInit_creal_T(&b_y, 1);
  n = x->size[1];
  emxInit_real_T(&costab1q, 2);
  emxInit_real_T(&costab, 2);
  emxInit_real_T(&sintab, 2);
  emxInit_real_T(&sintabinv, 2);
  emxInit_creal_T(&wwc, 1);
  emxInit_creal_T(&fv, 1);
  emxInit_creal_T(&b_fv, 1);
  if (x->size[1] == 0) {
    i = b_y->size[0];
    b_y->size[0] = x->size[1];
    emxEnsureCapacity_creal_T(b_y, i);
  } else {
    useRadix2 = ((x->size[1] & (x->size[1] - 1)) == 0);
    get_algo_sizes(x->size[1], useRadix2, &N2blue, &nd2);
    twid_re = 6.2831853071795862 / (double)nd2;
    j = nd2 / 2 / 2;
    i = costab1q->size[0] * costab1q->size[1];
    costab1q->size[0] = 1;
    costab1q->size[1] = j + 1;
    emxEnsureCapacity_real_T(costab1q, i);
    costab1q->data[0] = 1.0;
    nd2 = j / 2 - 1;
    for (ihi = 0; ihi <= nd2; ihi++) {
      costab1q->data[ihi + 1] = cos(twid_re * ((double)ihi + 1.0));
    }

    i = nd2 + 2;
    nd2 = j - 1;
    for (ihi = i; ihi <= nd2; ihi++) {
      costab1q->data[ihi] = sin(twid_re * (double)(j - ihi));
    }

    costab1q->data[j] = 0.0;
    if (!useRadix2) {
      j = costab1q->size[1] - 1;
      nd2 = (costab1q->size[1] - 1) << 1;
      i = costab->size[0] * costab->size[1];
      costab->size[0] = 1;
      costab->size[1] = nd2 + 1;
      emxEnsureCapacity_real_T(costab, i);
      i = sintab->size[0] * sintab->size[1];
      sintab->size[0] = 1;
      sintab->size[1] = nd2 + 1;
      emxEnsureCapacity_real_T(sintab, i);
      costab->data[0] = 1.0;
      sintab->data[0] = 0.0;
      i = sintabinv->size[0] * sintabinv->size[1];
      sintabinv->size[0] = 1;
      sintabinv->size[1] = nd2 + 1;
      emxEnsureCapacity_real_T(sintabinv, i);
      for (ihi = 0; ihi < j; ihi++) {
        sintabinv->data[ihi + 1] = costab1q->data[(j - ihi) - 1];
      }

      i = costab1q->size[1];
      for (ihi = i; ihi <= nd2; ihi++) {
        sintabinv->data[ihi] = costab1q->data[ihi - j];
      }

      for (ihi = 0; ihi < j; ihi++) {
        costab->data[ihi + 1] = costab1q->data[ihi + 1];
        sintab->data[ihi + 1] = -costab1q->data[(j - ihi) - 1];
      }

      i = costab1q->size[1];
      for (ihi = i; ihi <= nd2; ihi++) {
        costab->data[ihi] = -costab1q->data[nd2 - ihi];
        sintab->data[ihi] = -costab1q->data[ihi - j];
      }
    } else {
      j = costab1q->size[1] - 1;
      nd2 = (costab1q->size[1] - 1) << 1;
      i = costab->size[0] * costab->size[1];
      costab->size[0] = 1;
      costab->size[1] = nd2 + 1;
      emxEnsureCapacity_real_T(costab, i);
      i = sintab->size[0] * sintab->size[1];
      sintab->size[0] = 1;
      sintab->size[1] = nd2 + 1;
      emxEnsureCapacity_real_T(sintab, i);
      costab->data[0] = 1.0;
      sintab->data[0] = 0.0;
      for (ihi = 0; ihi < j; ihi++) {
        costab->data[ihi + 1] = costab1q->data[ihi + 1];
        sintab->data[ihi + 1] = -costab1q->data[(j - ihi) - 1];
      }

      i = costab1q->size[1];
      for (ihi = i; ihi <= nd2; ihi++) {
        costab->data[ihi] = -costab1q->data[nd2 - ihi];
        sintab->data[ihi] = -costab1q->data[ihi - j];
      }

      sintabinv->size[0] = 1;
      sintabinv->size[1] = 0;
    }

    if (useRadix2) {
      c_y = x->size[1] - 2;
      nInt2 = x->size[1] - 2;
      nRowsD2 = x->size[1] / 2;
      nRowsD4 = nRowsD2 / 2;
      i = b_y->size[0];
      b_y->size[0] = x->size[1];
      emxEnsureCapacity_creal_T(b_y, i);
      idx = 0;
      rt = 0;
      nd2 = 0;
      for (i = 0; i <= c_y; i++) {
        b_y->data[nd2].re = x->data[idx];
        b_y->data[nd2].im = 0.0;
        j = n;
        useRadix2 = true;
        while (useRadix2) {
          j >>= 1;
          rt ^= j;
          useRadix2 = ((rt & j) == 0);
        }

        nd2 = rt;
        idx++;
      }

      b_y->data[nd2].re = x->data[idx];
      b_y->data[nd2].im = 0.0;
      if (x->size[1] > 1) {
        for (i = 0; i <= nInt2; i += 2) {
          nt_re = b_y->data[i + 1].re;
          nt_im = b_y->data[i + 1].im;
          twid_im = b_y->data[i].re;
          twid_re = b_y->data[i].im;
          b_y->data[i + 1].re = b_y->data[i].re - b_y->data[i + 1].re;
          b_y->data[i + 1].im = b_y->data[i].im - b_y->data[i + 1].im;
          twid_im += nt_re;
          twid_re += nt_im;
          b_y->data[i].re = twid_im;
          b_y->data[i].im = twid_re;
        }
      }

      nd2 = 2;
      idx = 4;
      rt = ((nRowsD4 - 1) << 2) + 1;
      while (nRowsD4 > 0) {
        for (i = 0; i < rt; i += idx) {
          N2blue = i + nd2;
          nt_re = b_y->data[N2blue].re;
          nt_im = b_y->data[N2blue].im;
          b_y->data[N2blue].re = b_y->data[i].re - b_y->data[N2blue].re;
          b_y->data[N2blue].im = b_y->data[i].im - b_y->data[N2blue].im;
          b_y->data[i].re += nt_re;
          b_y->data[i].im += nt_im;
        }

        nInt2 = 1;
        for (j = nRowsD4; j < nRowsD2; j += nRowsD4) {
          twid_re = costab->data[j];
          twid_im = sintab->data[j];
          i = nInt2;
          ihi = nInt2 + rt;
          while (i < ihi) {
            N2blue = i + nd2;
            nt_re = twid_re * b_y->data[N2blue].re - twid_im * b_y->data[N2blue]
              .im;
            nt_im = twid_re * b_y->data[N2blue].im + twid_im * b_y->data[N2blue]
              .re;
            b_y->data[N2blue].re = b_y->data[i].re - nt_re;
            b_y->data[N2blue].im = b_y->data[i].im - nt_im;
            b_y->data[i].re += nt_re;
            b_y->data[i].im += nt_im;
            i += idx;
          }

          nInt2++;
        }

        nRowsD4 /= 2;
        nd2 = idx;
        idx += idx;
        rt -= nd2;
      }
    } else {
      nd2 = (x->size[1] + x->size[1]) - 1;
      i = wwc->size[0];
      wwc->size[0] = nd2;
      emxEnsureCapacity_creal_T(wwc, i);
      idx = x->size[1];
      rt = 0;
      wwc->data[x->size[1] - 1].re = 1.0;
      wwc->data[x->size[1] - 1].im = 0.0;
      nInt2 = x->size[1] << 1;
      i = x->size[1];
      for (ihi = 0; ihi <= i - 2; ihi++) {
        c_y = ((ihi + 1) << 1) - 1;
        if (nInt2 - rt <= c_y) {
          rt += c_y - nInt2;
        } else {
          rt += c_y;
        }

        nt_im = -3.1415926535897931 * (double)rt / (double)n;
        if (nt_im == 0.0) {
          nt_re = 1.0;
          nt_im = 0.0;
        } else {
          nt_re = cos(nt_im);
          nt_im = sin(nt_im);
        }

        wwc->data[idx - 2].re = nt_re;
        wwc->data[idx - 2].im = -nt_im;
        idx--;
      }

      idx = 0;
      i = nd2 - 1;
      for (ihi = i; ihi >= n; ihi--) {
        wwc->data[ihi] = wwc->data[idx];
        idx++;
      }

      nd2 = x->size[1];
      i = b_y->size[0];
      b_y->size[0] = x->size[1];
      emxEnsureCapacity_creal_T(b_y, i);
      idx = 0;
      for (ihi = 0; ihi < nd2; ihi++) {
        i = (n + ihi) - 1;
        b_y->data[ihi].re = wwc->data[i].re * x->data[idx];
        b_y->data[ihi].im = wwc->data[i].im * -x->data[idx];
        idx++;
      }

      i = x->size[1] + 1;
      for (ihi = i; ihi <= n; ihi++) {
        b_y->data[ihi - 1].re = 0.0;
        b_y->data[ihi - 1].im = 0.0;
      }

      r2br_r2dit_trig_impl(b_y, N2blue, costab, sintab, fv);
      r2br_r2dit_trig_impl(wwc, N2blue, costab, sintab, b_fv);
      i = b_fv->size[0];
      b_fv->size[0] = fv->size[0];
      emxEnsureCapacity_creal_T(b_fv, i);
      nd2 = fv->size[0];
      for (i = 0; i < nd2; i++) {
        twid_re = fv->data[i].re * b_fv->data[i].im + fv->data[i].im *
          b_fv->data[i].re;
        b_fv->data[i].re = fv->data[i].re * b_fv->data[i].re - fv->data[i].im *
          b_fv->data[i].im;
        b_fv->data[i].im = twid_re;
      }

      r2br_r2dit_trig_impl(b_fv, N2blue, costab, sintabinv, fv);
      if (fv->size[0] > 1) {
        twid_re = 1.0 / (double)fv->size[0];
        nd2 = fv->size[0];
        for (i = 0; i < nd2; i++) {
          fv->data[i].re *= twid_re;
          fv->data[i].im *= twid_re;
        }
      }

      idx = 0;
      i = wwc->size[0];
      for (ihi = n; ihi <= i; ihi++) {
        b_y->data[idx].re = wwc->data[ihi - 1].re * fv->data[ihi - 1].re +
          wwc->data[ihi - 1].im * fv->data[ihi - 1].im;
        b_y->data[idx].im = wwc->data[ihi - 1].re * fv->data[ihi - 1].im -
          wwc->data[ihi - 1].im * fv->data[ihi - 1].re;
        idx++;
      }
    }
  }

  emxFree_creal_T(&b_fv);
  emxFree_creal_T(&fv);
  emxFree_creal_T(&wwc);
  emxFree_real_T(&sintabinv);
  emxFree_real_T(&sintab);
  emxFree_real_T(&costab);
  emxFree_real_T(&costab1q);
  i = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = x->size[1];
  emxEnsureCapacity_creal_T(y, i);
  nd2 = x->size[1];
  for (i = 0; i < nd2; i++) {
    y->data[i] = b_y->data[i];
  }

  emxFree_creal_T(&b_y);
}

/* End of code generation (fft.c) */
