/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * inflection_point.c
 *
 * Code generation for function 'inflection_point'
 *
 */

/* Include files */
#include "inflection_point.h"
#include "extract.h"
#include "extract_emxutil.h"
#include "rt_nonfinite.h"
#include <math.h>

/* Function Definitions */
double inflection_point(const emxArray_real_T *s, double min_value, double
  max_value)
{
  int i;
  int i1;
  emxArray_real_T *b_y1;
  int dimSize_tmp;
  int orderForDim;
  emxArray_real_T *diff_value;
  int ixLead;
  int k;
  int iyLead;
  double work_data[2];
  double tmp1;
  double d;
  int m;
  boolean_T exitg1;
  double tmp2;
  if (min_value > max_value) {
    i = 0;
    i1 = 0;
  } else {
    i = (int)min_value - 1;
    i1 = (int)max_value;
  }

  emxInit_real_T(&b_y1, 1);
  dimSize_tmp = i1 - i;
  if (dimSize_tmp == 0) {
    b_y1->size[0] = 0;
  } else {
    orderForDim = dimSize_tmp - 1;
    if (orderForDim >= 2) {
      orderForDim = 2;
    }

    if (orderForDim < 1) {
      b_y1->size[0] = 0;
    } else {
      i1 = dimSize_tmp - orderForDim;
      ixLead = b_y1->size[0];
      b_y1->size[0] = i1;
      emxEnsureCapacity_real_T(b_y1, ixLead);
      if (i1 != 0) {
        ixLead = 1;
        iyLead = 0;
        work_data[0] = s->data[i];
        if (orderForDim >= 2) {
          tmp1 = s->data[i + 1];
          d = tmp1;
          tmp1 -= work_data[0];
          work_data[0] = d;
          work_data[1] = tmp1;
          ixLead = 2;
        }

        i1 = orderForDim + 1;
        for (m = i1; m <= dimSize_tmp; m++) {
          tmp1 = s->data[i + ixLead];
          for (k = 0; k < orderForDim; k++) {
            tmp2 = work_data[k];
            work_data[k] = tmp1;
            tmp1 -= tmp2;
          }

          ixLead++;
          b_y1->data[iyLead] = tmp1;
          iyLead++;
        }
      }
    }
  }

  emxInit_real_T(&diff_value, 1);
  ixLead = b_y1->size[0];
  i = diff_value->size[0];
  diff_value->size[0] = b_y1->size[0];
  emxEnsureCapacity_real_T(diff_value, i);
  for (k = 0; k < ixLead; k++) {
    diff_value->data[k] = fabs(b_y1->data[k]);
  }

  emxFree_real_T(&b_y1);
  ixLead = diff_value->size[0];
  if (diff_value->size[0] <= 2) {
    if (diff_value->size[0] == 1) {
      iyLead = 1;
    } else if ((diff_value->data[0] > diff_value->data[1]) || (rtIsNaN
                (diff_value->data[0]) && (!rtIsNaN(diff_value->data[1])))) {
      iyLead = 2;
    } else {
      iyLead = 1;
    }
  } else {
    if (!rtIsNaN(diff_value->data[0])) {
      iyLead = 1;
    } else {
      iyLead = 0;
      k = 2;
      exitg1 = false;
      while ((!exitg1) && (k <= diff_value->size[0])) {
        if (!rtIsNaN(diff_value->data[k - 1])) {
          iyLead = k;
          exitg1 = true;
        } else {
          k++;
        }
      }
    }

    if (iyLead == 0) {
      iyLead = 1;
    } else {
      tmp2 = diff_value->data[iyLead - 1];
      i = iyLead + 1;
      for (k = i; k <= ixLead; k++) {
        d = diff_value->data[k - 1];
        if (tmp2 > d) {
          tmp2 = d;
          iyLead = k;
        }
      }
    }
  }

  emxFree_real_T(&diff_value);
  return min_value + (double)iyLead;
}

/* End of code generation (inflection_point.c) */
